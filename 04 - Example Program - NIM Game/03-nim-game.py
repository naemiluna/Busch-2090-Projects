#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# @version   1.0 2017-06-17
# @copyright Copyright (c) 2017 Martin Sauter, martin.sauter@wirelessmoves.com
# @license   GNU General Public License v2
# @since     Since Release 1.0
# 
#
# Overview
# ########
#
# This Python program is a re-implementation of the Busch 2090 NIM game
# (PGM-7) for the PC and tries to be as close as possible to the 
# original implementation.
#
# The main differences are:
#
# * Calculations are much easier as there is no need for 4 bit additions
#   and subtrations and treating the carry bit (over/underflow)
# 
# * No conversion between HEX and DECIMAL for the display is necessary
#
# * Output and input mechanisms to communicate with the user is different.
#   The 2090 uses a single line 7 segment display and only shows numbers
#   while this program uses the shell, and outputs ASCII text, formatting
#   and debug information.
#
# Further detailed differences can be found in the code.
#
# There are around 35 Python instructions (excluding debug output) in this
# program vs. 68 pseudo-assembly instructions in the Busch 2090 variant.
#
# Background Information
# ######################
#
# The algorithm to win this game is described in German at
# http://www-i1.informatik.rwth-aachen.de/~algorithmus/algo14.php
# Alternative links can be found by searching for 
# 'Gewinnstrategie für ein Streichholzspiel'
#
# An English version seems to be only available in a book. Search 
# Google Books for'winning strategies for a matchstick game Jochen Könemann'
#
# As the book version is not freely available, Google Translate of the 
# German text linked to above might be helpful
# 



import sys

# Global variables
######################

num_sticks_left     = 0
max_num_sticks_take = 0


# Main program loop
######################

def main():

  #
  # Initialization part
  ######################
  
  print "Busch 2090 NIM Game - Python Implementation"
  print ""

  num_sticks_left = input('Enter number of sticks: ')
  
  # Busch 2090 comparison: Only 1-9 sticks can be removed (single digit), makes
  # calculation easier. There is no such limit here!
  max_num_sticks_take = input('Enter max. number of sticks that can be taken: ')

  while (num_sticks_left > 0):

    # User's move
    ######################
    
    print ' '
    print '############################'
    print ' '    
    
    sticks_to_take = input ('User move, how many sticks to take?: ')
    
    if (sticks_to_take > max_num_sticks_take):
      print 'Error, number of sticks too great, exiting'
      sys.exit()
       
    if (sticks_to_take <= 0):
      print 'Error, number of sticks to take < 1, exiting'
      sys.exit()
    
    # Remove requested number of stick
    num_sticks_left = num_sticks_left - sticks_to_take
    
    print 'Sticks left: ', num_sticks_left
    
    if (num_sticks_left == 1):
      print 'YOU HAVE WON!'
      sys.exit()
      
    else:

      # Computer's move
      ######################
      
      # Busch 2090 comparison: starts with sticks_to_take = 
      # max_num_sticks_take + 1 and implements a do-while loop. 
      # Python doesn't have do-while loops so the start 
      # without the +1 and reduce at the end of the loop
      # instead of at the beginning.
      #
      # The inner+outer loops below use the following loop
      # variables:
      #
      # sticks_to_take (decreased in the outer loop)
      # save_loc_to_hit (increased in the inner loop)
      # temp_num_sticks_left (result of calculation)
      #
      # After both loops have finished
      #
      #    num_sticks_left
      #
      # is set to the new value from which the user then continues
      # Unlike in the Busch 2090 implementation there is no 
      # number of sticks taken variable that is shown on the display
      # so this variable is not updated.
      
      sticks_to_take = max_num_sticks_take
           
      while (sticks_to_take > 0):
      
        save_loc_to_hit = 1
        temp_num_sticks_left = num_sticks_left - sticks_to_take
        
        print 'Removing', sticks_to_take, 'sticks and starting with', \
              temp_num_sticks_left, 'sticks left!'
  
        # Try to find a save location with the current number of removed sticks
        #
        # Busch 2090 comparison:
        #
        # 1. Here, the loop continues while the save location to hit is smaller
        #    than the number of temporary sticks left. On the Busch 2090, 
        #    16 attemps (4 bit counter) are made and an equal comparison is
        #    made. This means that the save_loc_to_hit can and most often will
        #    exceede the number of sticks left. This was probably done so only
        #    the EQUAL comparison has to be done while the loop has always
        #    16 iterations if now save position is found!
        #
        # 2. The temp_num_sticks_left can become negative at the end while
        #    on the Busch 2090, there are no negative numbers and a wrap
        #    around to a high 4 bit number would occur. This has no impact
        #    on the outcome.
        
        while (temp_num_sticks_left > save_loc_to_hit):
        
          # Have we reached a save position?
          if (save_loc_to_hit == temp_num_sticks_left):
            print 'Save location found: ', save_loc_to_hit
            # Busch 2090 comparison: We have to do a break here and a
            # break in the outer loop as well after finding a save
            # location to get out of the double loop construct. In the 
            # Busch 2090 code for this state (memory location 0x28) a
            # direct branch is made to exit both loops!
            break

          # Try next save location
          save_loc_to_hit = save_loc_to_hit + max_num_sticks_take + 1
          print 'Next save location: ', save_loc_to_hit
          
        # end of loop that tries to find a save location with the CURRENT
        # number of sticks to remove
        #####################################################################

        # If the inner loop has found a save location stop looking 
        # for other solutions with fewer sticks        
        if (save_loc_to_hit == temp_num_sticks_left):
          break

        # Let's try again with one less stick
        sticks_to_take = sticks_to_take - 1
        print '----'
        

      # End of sticks count down loop
      #####################################################################

      # Check the result of the computations above      
      if (temp_num_sticks_left == save_loc_to_hit):
        num_sticks_left = temp_num_sticks_left
        print 'Save location found!'
        print 'Computer takes', sticks_to_take, 'sticks. Sticks still left: ', \
              num_sticks_left

      if (num_sticks_left == 1):
        print 'COMPUTER HAS WON!'
        sys.exit()     
        
      if (temp_num_sticks_left != save_loc_to_hit):
        print 'No save location found'
        num_sticks_left = num_sticks_left - 1
        print 'Computer takes 1 stick. Sticks still left: ', num_sticks_left
                           
  # End of main loop, go back and let the user make the next move!
  #####################################################################


if __name__ == '__main__':
  main()
