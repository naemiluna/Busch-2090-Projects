#!/usr/bin/python

#
# @version   1.0 2017-06-05
# @copyright Copyright (c) 2017 Martin Sauter, martin.sauter@wirelessmoves.com
# @license   GNU General Public License v2
# @since     Since Release 1.0
# 
#
# Overview
# ########
#
# This program emulates a Busch 2095 tape interface for loading a
# program FROM tape TO the Busch 2090 microtronic computer system
# with 3 GPIO pins of a Raspberry Pi. 
#
# Note: The program is based on observations made while performing
# data transfers from the 2090 to the tape interface and monitoring 
# the 2090 outputs while the 2090 inputs were toggled.
#
# The program has been written for the Raspberry Pi 2 and 3 with 
# long GPIO connectors. It can also be used with an older 
# Raspberry Pi by adapting the GPIO ports used.
#
# Operation
# #########
#
# A program is loaded from the emulated tape to the memory of the 
# Busch 2090 as follows:
#
# 1. Enter HALT - PGM5 to initialize all memory to 00
#
# 2. Enter PGM1 to start the 'read from tape' program of the 2090
#
# The 2090 will now wait for data from the tape interface.
#
# 3. Start this program with the name of the file that contains
#    the program to load.
#
# The Raspberry Pi simulated tape interface uses 3 OUTPUT lines (+GND)
# on the Raspberry Pi connected to 3 INPUT lines on the Busch 2090
# to send the program data as follows:
#
# 1. Each bit of tripple nibble is sent to the Busch 2090 on OUT1 in
#    a loop that repeats 3x4 = 12 times. OUT1 is set to HIGH or LOW
#    at the beginning of each loop (bit) cycle.
#    
# 2. The start of a new instruction nibble triplet (3x4 bits) is
#    signaled by raising OUT3 (Connected to IN3 on the Busch 2090)
#    after the first bit has been set in OUT1. OUT3 remains HIGH
#    throughout the 3x4bit transmission.
#
# 3. Every subsequent bit (11 in total) is then put on OUT1. Once done,
#    OUT2 is toggled from LOW to HIGH for 10 milliseconds and then back 
#    to LOW to signal the Busch 2090 that the next bit is ready.
#
#    Note: OUT2 is not used for the first bit but only for the subsequent
#    11 others. The readiness of the first bit is signaled by raising
#    OUT3 as described in step 2.
#
# 4. At the end of the 3x4 bits instruction OUT3 is set to LOW again to
#    signal the end of the transmission. OUT1 is also set to LOW.
#
# 5. A 10ms delay allows the Busch 2090 to store the received instruction
#    to memory.
#
# 6. Steps 1-5 are repeated for each instruction code contained in
#    file.
#
# 7. The program ends when all instructions have been transmitted to
#    the Busch 2090. On the 2090, press the RESET button to end PGM1
#    as it only ends automatically in case all 0xFF memory locations
#    have been transmitted.
#
# 8. Execute the loaded program with HALT - NEXT - 0 - 0 - RUN
#
# Note: A HEF4081 4x dual AND gate is used to level-shift the 3.3V signals
#       of Raspberry Pi GPIOs to 5V!
#

import RPi.GPIO as GPIO # Import GPIO Library
import time,sys        

# Output ports
out_1 = 16
out_2 = 20
out_3 = 21

# Delays
delay_bit          = 0.01
delay_after_value  = 0.01

##############################################################################
#
# clockSignal()
#
# This function is used to produce a quick clock signal by raising the given
# pin to HIGH for half of the delay given. The pin is then set to LOW again.
# The function then waits the second half of the given delay and returns.
#
# @param pin, Raspberry Pi output pin to use for the operation
# @parm  delay, in seconds (e.g. 0.01 = 10 milliseconds)
#
# @return none
#
##############################################################################

def clockSignal (pin, delay):
  GPIO.output (pin, GPIO.HIGH)     
  time.sleep (delay/2)                
  GPIO.output (pin, GPIO.LOW)
  time.sleep (delay/2)
  return;


##############################################################################
#
# initializeGPIOsForBusch2090()
#
# @return none
#
##############################################################################

def initGPIOsForBusch2090():

  GPIO.setmode (GPIO.BCM) # Use BOARD pin numbering

  # set output ports
  GPIO.setup (out_1, GPIO.OUT)  
  GPIO.setup (out_2, GPIO.OUT)  
  GPIO.setup (out_3, GPIO.OUT)  

  GPIO.output (out_1, GPIO.LOW)
  GPIO.output (out_2, GPIO.LOW)
  GPIO.output (out_3, GPIO.LOW)


##############################################################################
#
# loadInstructionToBusch2090()
#
# This function emulates the behavior of the Busch 2095 tape interface
# card and loads ONE instruction nibble triplet to the Busch 2090 
# microtronic computer. Details on how this is done can be found in the
# overview description of the file
#
# @param string, 3 character instruction code
#
##############################################################################

def loadInstructionToBusch2090(in_str):

  # Loop over each of the 3 nibbles (4 bits) of the current instruction
  for y in range(0, 3):

    # Set the mask that is used to check if a bit is 0 or 1
    # to the first bit.
    mask = 1
    
    # Loop over each bit of the current nibble
    for z in range(0, 4):
    
      # If the current bit is 1
      if int(in_str[y], 16) & mask > 0:
        # Output a HIGH bit
        GPIO.output (out_1, GPIO.HIGH)
      else:
        # Output a LOW bit
        GPIO.output (out_1, GPIO.LOW)
    
      # If this is the first bit of the 3 nibble instruction
      if (y == 0 and z == 0):
        # Raise OUT3 to signal start of a new instruction
        GPIO.output (out_3, GPIO.HIGH)
        time.sleep(delay_bit)          
      else:
        # All other bits are signaled with a clock tick on OUT2
        clockSignal (out_2, delay_bit)           
        
      # Move the 'mask' bit one location forward  
      mask = mask << 1
        
  # End of current 3 x 4 byte transmission

  # Set all OUTs to 0 to signal the end of the current 3 nibble instruction
  GPIO.output (out_3, GPIO.LOW)
  GPIO.output (out_2, GPIO.LOW)
  GPIO.output (out_1, GPIO.LOW)
  
  # Give the 2090 some time to write the received nibbles to memory    
  time.sleep(delay_after_value)

#end of loadInstructionToBusch2090()


##############################################################################
#
# main()
#
# * The main function of this module opens the file given as first parameter.
#   If no filename is given an error message is returned to the user and
#   the program ends.
#
# * If the file is found it initializes the Raspberry Pi's GPIO interface
#
# * The function then an reads each line and if it contains a Busch 2090
#   instruction (3 character instruction code) it calls the function
#   to send to code to the 2090.
#
##############################################################################

def main():

  # Get filename from which to read the Busch 2090 instructions
  if len(sys.argv) < 2:
    print "Error: Filename not given"
    sys.exit()
 
  try:
    fh = open(sys.argv[1], "r")

  except IOError:
    print "Unable to open file '" + sys.argv[1] + "'"
    sys.exit()

  # File exists, initialize Raspi I/O  ports
  initGPIOsForBusch2090()
      
  x = 0 # Initialize the instruction number counter

  # Loop over all Busch 2090 instructions
  #############################################

  for line in fh.readlines():

    try:

      # Get current instruction  
      # Exclude lines that do not contain an instruction code
      if len(line) < 3: continue
      if line[0] == " ": continue
      if line[0].isalpha() != True and line[0].isdigit() != True: continue
    
      # Found an instruction code 
      in_str = line[0:3]
      
      # Search/replace OCR code errors,
      # see https://github.com/lambdamikel/Busch-2090/tree/master/software
      in_str = in_str.replace ("I", "1")
      in_str = in_str.replace ("l", "1")
      in_str = in_str.replace ("Q", "0")
      in_str = in_str.replace ("O", "0")

      print format(x, '#04x') + ": " + in_str

      # SEND 3 x 4 BYTE INSTRUCTION TO THE BUSCH 2090
      loadInstructionToBusch2090(in_str)
      
      # Increase instruction counter
      x = x + 1
  
    except KeyboardInterrupt:
      break
    
   # end of 'for each line' loop
    
  GPIO.cleanup()
  fh.close();


if __name__ == '__main__':
  main()


