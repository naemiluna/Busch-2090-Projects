# Busch-2090-Projects
A Raspberry Pi based Tape Emulator, an Assembler and other tidbits for the Busch 2090 historical microcomputer

![Project Image](https://gitlab.com/heurekus/Busch-2090-Projects/raw/master/03%20-%20PGM1%20-%20read%20from%20tape%20emulated%20with%20a%20Raspberry%20Pi/02%20pictures/04%20-%202017%2006%2010%20-%202090%20Tape%20Emulator%20-%20Integrated%20Setup-small.jpg)

### Overview

This repository contains software, circuit diagrams and background information for several Busch 2090 related projects. Each project is contained in a separate directory:

* **_01 - first trace setup_**: Documents the first steps to explore the (digital) interface between the Busch 2090 computer and the 2095 cassette interface

* **_02 - PGM2 - record to tape - timing of signals - triggered with a Raspbery Pi_**: Contains a Python program and background information of how the 2090 can be triggered via a Raspberry Pi to send program data stored in its memory to the 2095 cassette / tape interface. This was only an intermediary step, the data was never read on the Raspberry Pi but just evaluated with a logic analyzer.

* **_03 - PGM1 - read from tape emulated with a Raspberry Pi_**: This is the MAIN directory that contains the Python program to download software from the Raspberry Pi to the Busch 2090, some sample programs to download and information on how to build the hardware to connect the Raspberry Pi via some of its GPIO ports to the 2090. Note: [here](https://youtu.be/7KmG64e4DPE) is a video of how this process worked with a real casette tape.

* **_04 - Example Program - NIM Game_**: An example program to download to the 2090 via PGM1, a detailed commented version of it, a commented version in Libreoffice Calc format from which column B can be used to download to the 2090 and a Python version that runs on the PC.

* **_05 - Busch 2090 Assembler_**: A cross-platform assembler to conveniently translate Busch 2090 assembly instructions into 3-digit hex code machine code that can be downloaded to the original hardware with the tape emulator.

* **_07 - Micsim_**: A Busch 2090 for Linux (PC) emulator written by Ingo Rullhusen. GPL source and executable made available here are a perfect companion to test programms witten with the Busch 2090 Assembler from the directory above.

* **_08 - Busch 2090 Serial Numbers_**: A collection of serial numbers that give an idea of the number of sold devices.

### Resources:

Further background information such as PDFs of the original manuals and software to download to the 2090 is available at https://github.com/lambdamikel/Busch-2090/tree/master/software
