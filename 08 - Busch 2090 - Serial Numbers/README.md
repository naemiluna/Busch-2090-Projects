# Busch 2090 Serial Numbers


![Busch 2090 serial number images](https://gitlab.com/heurekus/Busch-2090-Projects/tree/master/08%20-%20Busch%202090%20-%20Serial%20Numbers/details%20per%20serial%20number)

### Overview

 * Each Busch 2090 contains a number of identifying numbers that give a clue as to when and how many of them were produced over time:
 
 * Each device has a serial number sticker at the BOTTOM of the casing. The following list contains all serial numbers known to the author of his own devices and those reported by others. Directories with serial numbers contain additional information and contain images of that particular device if the information was released by the author. Assuming serial numbers were assinged sequentially the number would reflect the minimum number of devices sold at the time.
 
 * On the TMS-1600 CPU, the number in the second line of the text on the chip could be the production year and week (e.g. 8131 = 1981, week 31). This is speculative at this point.
 
 * On the motherboard there is a revision number in the lower left corner. It can only be seen after removing the two screws on the left and right side of the front cover and then carefully lifting the front cover and removing the styropor spacing at that location. On later models the production month and year might be indicated in the last 4 digits while earlier versions had a different numbering scheme.
 
 * On the motherboard some models have a sticker with a date in the lower right corner. This can only be seen by unscrewing and lifting the front cover and looking under the keyboard.
 
 
| Serial Number        | TMS-1600 prod. date (YYWW-?) | Mainb. ID (left) | Mainb. date (right) | Info           | 
| ------------- | ---- | ---- | ---- |:-------------| 
| -      | 8131 | | |Microtronic brochure, 1981 |
| 106873 | 8139 | F 0604/4| 09.11.81 | Lilly | 
| 107344 | 8139 |         |  |Seen on eBay 28. November 2017|
| 107383 | 8139 | F 0601/4| 21.11.81 |Martin Sauter - 3 - https://blog.wirelessmoves.com|
| 109149 | 8221 | | | Michael Wessel -  1 - https://www.michael-wessel.info/ | 
| 109728 | 8203 |  | |Seen on eBay, October 2017 |
| -      | 8221 | F 0601/6| (not present) | Martin Sauter - 1 (no casing) - https://blog.wirelessmoves.com|
| - | 8221 | F 0601/6 | | Michael Wessel (see 'no casing - no serial 2') |
| 111240 | 8222 |  | |Seen on eBay, August 2017 |
| 112107 | 8146 |  | | |
| 113985 | 8241 |  | |Seen during VCFB 2018 |
| 114078 | 8243 |  | | Michael Wessel - 3 |
| 114375 | 8147 |  | | Michael Wessel - 2 - https://www.michael-wessel.info/ |
| 114492 | 8241 | 7.70.2090-1283 | (not present) |Martin Sauter - 2 - https://blog.wirelessmoves.com|

* Delta from first to last: 7619
