/*
*   mic2wav.c
*/
#define VERSION "23.7.2005"
/*
*   convert MIC file into WAV file for Kassentten-Interface 2095
*/
#define CPR "(c) 2005 Dr.-Ing. Ingo D. Rullhusen <d01c@uni-bremen.de>"
/*
* This program is free software; you can redistribute it and/or modify   
* it under the terms of the GNU General Public License as published by   
* the Free Software Foundation; either version 2 of the License, or   
* (at your option) any later version.   
* 
* This program is distributed in the hope that it will be useful,   
* but WITHOUT ANY WARRANTY; without even the implied warranty of   
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
* GNU General Public License for more details.   
*
* You should have received a copy of the GNU General Public License   
* along with this program; if not, write to the Free Software   
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*
*/


#include <stdio.h>


#define FREQ_0  568.0
#define FREQ_1 1136.0
#define FREQ_2 2272.0

#define T_INIT 10.0

#define T_BIT   0.063
#define T_BIT_F 0.047

#define T_SYNC_0  0.030
#define T_SYNC_1  0.170

#define T_END 1.0


#define SAMPLE_RATE 22050

#define HI_STATE 254
#define LO_STATE 2


unsigned char hex2ui( char hex )
{
   switch( hex )
   {
      case '0': { return(0x0); }
      case '1': { return(0x1); }
      case '2': { return(0x2); }
      case '3': { return(0x3); }
      case '4': { return(0x4); }
      case '5': { return(0x5); }
      case '6': { return(0x6); }
      case '7': { return(0x7); }
      case '8': { return(0x8); }
      case '9': { return(0x9); }
      case 'A':
      case 'a': { return(0xa); }
      case 'B':
      case 'b': { return(0xb); }
      case 'C':
      case 'c': { return(0xc); }
      case 'D':
      case 'd': { return(0xd); }
      case 'E':
      case 'e': { return(0xe); }
      case 'F':
      case 'f': { return(0xf); }
   }
   fprintf(stderr,"MIC file not valid!\n");
   return(0xff);
}


struct WAV_HEADER
{
   char riff[4];
   unsigned int  f_length_m8; /* to be set */
   char wave[4];
   char fmt_[4];
   unsigned int  fmt_length;
   unsigned short format;
   unsigned short channels;
   unsigned int sample_rate;
   unsigned int byte_per_sec;
   unsigned short block_align;
   unsigned short bits_per_sample;
   char data[4];
   unsigned int data_length; /* to be set */
} wav_header = { { 'R', 'I', 'F', 'F' },
                 0,
                 { 'W', 'A', 'V', 'E' },
                 { 'f', 'm', 't', ' ' },
                 16,
                 1,
                 1,
                 SAMPLE_RATE,
                 SAMPLE_RATE,
                 1,
                 8,
                 { 'd', 'a', 't', 'a' },
                 0 };


void write_wave( FILE *wavfile, double dur, double freq )
{
   static unsigned char lvl = HI_STATE;
   static unsigned long int rpos = 0;

   unsigned long int pos;
   double time, rtime, itv;

   itv = 0.5/freq;

   pos = 0;
   time = 0.0;

   while ( time < dur )
   {
      fwrite((void*)&lvl,sizeof(lvl),1,wavfile);

      pos++;
      time = pos / (double)SAMPLE_RATE;

      rpos++;
      rtime = rpos / (double)SAMPLE_RATE;
      if ( rtime > itv )
      {
         rpos = 0;
         if ( lvl == HI_STATE ) lvl = LO_STATE;
         else                   lvl = HI_STATE;
      }
   }
}


void write_init( FILE *wavfile )
{
   write_wave( wavfile, T_INIT, FREQ_2 );
}

void write_line( FILE *wavfile, char c1, char c2, char c3 )
{
   unsigned char ui;

   ui = hex2ui( c1 );

   if ( ui & 0x1 ) write_wave( wavfile, T_BIT_F, FREQ_1 );
   else            write_wave( wavfile, T_BIT_F, FREQ_0 );

   if ( ui & 0x2 ) write_wave( wavfile, T_BIT, FREQ_1 );
   else            write_wave( wavfile, T_BIT, FREQ_0 );

   if ( ui & 0x4 ) write_wave( wavfile, T_BIT, FREQ_1 );
   else            write_wave( wavfile, T_BIT, FREQ_0 );

   if ( ui & 0x8 ) write_wave( wavfile, T_BIT, FREQ_1 );
   else            write_wave( wavfile, T_BIT, FREQ_0 );


   ui = hex2ui( c2 );

   if ( ui & 0x1 ) write_wave( wavfile, T_BIT, FREQ_1 );
   else            write_wave( wavfile, T_BIT, FREQ_0 );

   if ( ui & 0x2 ) write_wave( wavfile, T_BIT, FREQ_1 );
   else            write_wave( wavfile, T_BIT, FREQ_0 );

   if ( ui & 0x4 ) write_wave( wavfile, T_BIT, FREQ_1 );
   else            write_wave( wavfile, T_BIT, FREQ_0 );

   if ( ui & 0x8 ) write_wave( wavfile, T_BIT, FREQ_1 );
   else            write_wave( wavfile, T_BIT, FREQ_0 );


   ui = hex2ui( c3 );

   if ( ui & 0x1 ) write_wave( wavfile, T_BIT, FREQ_1 );
   else            write_wave( wavfile, T_BIT, FREQ_0 );

   if ( ui & 0x2 ) write_wave( wavfile, T_BIT, FREQ_1 );
   else            write_wave( wavfile, T_BIT, FREQ_0 );

   if ( ui & 0x4 ) write_wave( wavfile, T_BIT, FREQ_1 );
   else            write_wave( wavfile, T_BIT, FREQ_0 );

   if ( ui & 0x8 ) write_wave( wavfile, T_BIT, FREQ_1 );
   else            write_wave( wavfile, T_BIT, FREQ_0 );

}

void write_sync( FILE *wavfile )
{
   write_wave( wavfile, T_SYNC_0, FREQ_0 );
   write_wave( wavfile, T_SYNC_1, FREQ_2 );
}

void write_end( FILE *wavfile )
{
   write_wave( wavfile, T_END, FREQ_2 );
}


int main( int argc, char *argv[] )
{
   int lno;
   char buff[4];
   FILE *micfile, *wavfile;

   if ( argc != 3 )
   {
      printf("mic2wav (version %s)\n",VERSION);
      printf(" Copyright %s\n",CPR);
      printf(" mic2wav comes with ABSOLUTELY NO WARRANTY; for details see GNU\n");
      printf(" GENERAL PUBLIC LICENSE. This is free software, and you are welcome to\n");
      printf(" redistribute it under certain conditions; see GNU GENERAL PUBLIC LICENSE\n");
      printf(" for details.\n\n");

      printf("mic2wav <MIC file> <WAV file>\n");
      return(0);
   }

   micfile = fopen( argv[1], "r" );
   if ( ! micfile )
   {
      fprintf(stderr,"Cannot open file %s!\n",argv[1]);
      return(2);
   }

   wavfile = fopen( argv[2], "w" );
   if ( ! wavfile )
   {
      fprintf(stderr,"Cannot open file %s!\n",argv[2]);
      return(2);
   }


   fwrite((void*)&wav_header,sizeof(wav_header),1,wavfile);

   for ( lno=0; lno<256; lno++ )
   {
      buff[0] = (char)getc( micfile );
      buff[1] = (char)getc( micfile );
      buff[2] = (char)getc( micfile );
      buff[3] = (char)getc( micfile );

      if ( feof(micfile) )
      {
         fprintf(stderr,"MIC file incomplete!\n");
         return(5);
      }

      if ( buff[3] != '\r' )
      {
         fprintf(stderr,"MIC file inconsistent!\n");
         return(5);
      }

      if ( lno == 0 ) write_init( wavfile );
      else            write_sync( wavfile );

      write_line( wavfile, buff[0], buff[1], buff[2] );
   }

   write_end( wavfile );

   wav_header.f_length_m8 = ftell(wavfile) - 8;
   wav_header.data_length = ftell(wavfile) - sizeof(wav_header);

   fseek(wavfile, 0, SEEK_SET);
   fwrite((void*)&wav_header,sizeof(wav_header),1,wavfile);

   fclose(wavfile);
   fclose(micfile);
   return(0);
}
