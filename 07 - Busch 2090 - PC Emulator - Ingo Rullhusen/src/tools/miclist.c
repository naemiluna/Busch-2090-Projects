/*
*   miclist.c
*/
#define VERSION "20.8.2005"
/*
*   list the contents of a MIC file
*/
#define CPR "(c) 2005 Dr.-Ing. Ingo D. Rullhusen <d01c@uni-bremen.de>"
/*
* This program is free software; you can redistribute it and/or modify   
* it under the terms of the GNU General Public License as published by   
* the Free Software Foundation; either version 2 of the License, or   
* (at your option) any later version.   
* 
* This program is distributed in the hope that it will be useful,   
* but WITHOUT ANY WARRANTY; without even the implied warranty of   
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
* GNU General Public License for more details.   
*
* You should have received a copy of the GNU General Public License   
* along with this program; if not, write to the Free Software   
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*
*/


#include <stdio.h>

void mnemonic( char* M, char c1, char c2, char c3 )
{
   switch( c1 )
   {
      case '0':
      {
         sprintf(M,"MOV  %c,%c",c2,c3);
         break;
      }
      case '1':
      {
         sprintf(M,"MOVI %c,%c",c2,c3);
         break;
      }
      case '2':
      {
         sprintf(M,"AND  %c,%c",c2,c3);
         break;
      }
      case '3':
      {
         sprintf(M,"ANDI %c,%c",c2,c3);
         break;
      }
      case '4':
      {
         sprintf(M,"ADD  %c,%c",c2,c3);
         break;
      }
      case '5':
      {
         sprintf(M,"ADDI %c,%c",c2,c3);
         break;
      }
      case '6':
      {
         sprintf(M,"SUB  %c,%c",c2,c3);
         break;
      }
      case '7':
      {
         sprintf(M,"SUBI %c,%c",c2,c3);
         break;
      }
      case '8':
      {
         sprintf(M,"CMP  %c,%c",c2,c3);
         break;
      }
      case '9':
      {
         sprintf(M,"CMPI %c,%c",c2,c3);
         break;
      }
      case 'A':
      {
         sprintf(M,"OR   %c,%c",c2,c3);
         break;
      }
      case 'B':
      {
         sprintf(M,"CALL %c%c",c2,c3);
         break;
      }
      case 'C':
      {
         sprintf(M,"GOTO %c%c",c2,c3);
         break;
      }
      case 'D':
      {
         sprintf(M,"BRC  %c%c",c2,c3);
         break;
      }
      case 'E':
      {
         sprintf(M,"BRZ  %c%c",c2,c3);
         break;
      }

      case 'F':
      {
         switch(c2)
         {
            case '0':
            {
               switch (c3)
               {
                  case '0':
                  {
                     sprintf(M,"HALT");
                     break;
                  }
                  case '1':
                  {
                     sprintf(M,"NOP");
                     break;
                  }
                  case '2':
                  {
                     sprintf(M,"DISOUT");
                     break;
                  }
                  case '3':
                  {
                     sprintf(M,"HXDZ");
                     break;
                  }
                  case '4':
                  {
                     sprintf(M,"DZHX");
                     break;
                  }
                  case '5':
                  {
                     sprintf(M,"RND");
                     break;
                  }
                  case '6':
                  {
                     sprintf(M,"TIME");
                     break;
                  }
                  case '7':
                  {
                     sprintf(M,"RET");
                     break;
                  }
                  case '8':
                  {
                     sprintf(M,"CLEAR");
                     break;
                  }
                  case '9':
                  {
                     sprintf(M,"STC");
                     break;
                  }
                  case 'A':
                  {
                     sprintf(M,"RSC");
                     break;
                  }
                  case 'B':
                  {
                     sprintf(M,"MULT");
                     break;
                  }
                  case 'C':
                  {
                     sprintf(M,"DIV");
                     break;
                  }
                  case 'D':
                  {
                     sprintf(M,"EXRL");
                     break;
                  }
                  case 'E':
                  {
                     sprintf(M,"EXRM");
                     break;
                  }
                  case 'F':
                  {
                     sprintf(M,"EXRA");
                     break;
                  }

                  default:
                  {
                     sprintf(M,"!!! ERROR !!!");
                  }
               }

               break;
            }

            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            {
               sprintf(M,"DISP %c,%c",c2,c3);
               break;
            }

            case '7':
            {
               sprintf(M,"MAS  %c",c3);
               break;
            }
            case '8':
            {
               sprintf(M,"INV  %c",c3);
               break;
            }
            case '9':
            {
               sprintf(M,"SHR  %c",c3);
               break;
            }
            case 'A':
            {
               sprintf(M,"SHL  %c",c3);
               break;
            }
            case 'B':
            {
               sprintf(M,"ADC  %c",c3);
               break;
            }
            case 'C':
            {
               sprintf(M,"SUBC %c",c3);
               break;
            }
            case 'D':
            {
               sprintf(M,"DIN  %c",c3);
               break;
            }
            case 'E':
            {
               sprintf(M,"DOT  %c",c3);
               break;
            }
            case 'F':
            {
               sprintf(M,"KIN  %c",c3);
               break;
            }

            default:
            {
               sprintf(M,"!!! ERROR !!!");
            }
         }

         break;
      }

      default:
      {
         sprintf(M,"!!! ERROR !!!");
      }
   }
}


int main( int argc, char *argv[] )
{
   int lno;
   char buff[4];
   char M[16];
   FILE *micfile;

   if ( argc != 2 )
   {
      printf("miclist (version %s)\n",VERSION);
      printf(" Copyright %s\n",CPR);
      printf(" miclist comes with ABSOLUTELY NO WARRANTY; for details see GNU\n");
      printf(" GENERAL PUBLIC LICENSE. This is free software, and you are welcome to\n");
      printf(" redistribute it under certain conditions; see GNU GENERAL PUBLIC LICENSE\n");
      printf(" for details.\n\n");

      printf("miclist <MIC file>\n");
      return(0);
   }

   micfile = fopen( argv[1], "r" );
   if ( ! micfile )
   {
      fprintf(stderr,"Cannot open file %s!\n",argv[1]);
      return(2);
   }

   for ( lno=0; lno<256; lno++ )
   {
      buff[0] = (char)getc( micfile );
      buff[1] = (char)getc( micfile );
      buff[2] = (char)getc( micfile );
      buff[3] = (char)getc( micfile );

      if ( feof(micfile) )
      {
         fprintf(stderr,"MIC file incomplete!\n");
         return(5);
      }

      if ( buff[3] != '\r' )
      {
         fprintf(stderr,"MIC file inconsistent!\n");
         return(5);
      }

      mnemonic( M, buff[0],buff[1],buff[2] );

      printf("%.2X %c%c%c  :  %s\n",lno,
                                    buff[0],buff[1],buff[2],
                                    M );

   }

   fclose(micfile);
   return(0);
}
