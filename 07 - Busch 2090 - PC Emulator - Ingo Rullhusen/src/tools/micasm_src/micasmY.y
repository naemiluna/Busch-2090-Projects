/*
*   micasmY.y
*
*   version 27.8.2005
*   (c) 2005 Dr.-Ing. Ingo D. Rullhusen <d01c@uni-bremen.de>
*
* This program is free software; you can redistribute it and/or modify   
* it under the terms of the GNU General Public License as published by   
* the Free Software Foundation; either version 2 of the License, or   
* (at your option) any later version.   
* 
* This program is distributed in the hope that it will be useful,   
* but WITHOUT ANY WARRANTY; without even the implied warranty of   
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
* GNU General Public License for more details.   
*
* You should have received a copy of the GNU General Public License   
* along with this program; if not, write to the Free Software   
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*
*/


%{

#include <stdlib.h>
#include <ctype.h>
#include <string.h>


char memory[256][3];

char *filename;
int lno;
char fill[3];
char *label[256], *pre_label[256];

int error_line = 0;
char *error_msg = NULL;


int yyerror(char *s);

extern int yylex();


%}


%start file


%union { char chr;
         char*  chstr;
         char chr3[3];
       }
%type <chr3> opx addr hexno2 hexno3
%type <chr>  hexno char_1_6

%token <chr>   CHAR_0 CHAR_1_6 CHAR_7_F
%token <chstr> QUOTED LABEL

%token COMMEND COMMA FILL OUTPUT COLON

%token MOV MOVI AND ANDI ADD ADDI SUB SUBI CMP CMPI OR
%token CALL GOTO BRC BRZ
%token HALT NOP DISOUT HXDZ DZHX RND TIME RET CLEAR STC RSC
%token MULT DIV EXRL EXRM EXRA
%token DISP MAS INV SHR SHL ADC SUBC DIN DOT KIN

%left NEWL
%right EQ NBR


%%


file : lines
     ;

lines : lines line
      | /* empty */
      ;

line : NEWL
     | def
     | code
     | COMMEND
     ;


def : FILL EQ hexno3 NEWL
      {
         fill[0] = (char)toupper($3[0]);
         fill[1] = (char)toupper($3[1]);
         fill[2] = (char)toupper($3[2]);
      }
    | OUTPUT EQ QUOTED NEWL
      {
         free(filename);
         filename = calloc( sizeof(char), strlen($3)+1 );
         strcpy( filename, $3+1 );
         filename[strlen(filename)-1]  = '\0';
      }
    | hexno2 EQ LABEL NEWL
      {
         int no;
         no = hex2ui( $1[1] )*16 + hex2ui( $1[2] );
         if ( label[no] != NULL )
         {
            fprintf(stderr,"Multiple assigned label!\n");
         }
         label[no] = calloc( sizeof(char), strlen($3)+1 );
         strcpy( label[no], $3 );
      }
    ;

code : LABEL COLON
       {
          int i;
          for ( i=0; i<256; i++ )
          {
             if ( label[i] )
             {
                if ( strstr(label[i], $1) && strstr( $1, label[i] ) ) break;
             }
          }
          if ( i < 256 ) /* label found */
          {
             if ( i < lno )
             {
                fprintf(stderr,"Label address %.2X too small!\n",i);
             }
             for ( ; lno<i; lno++ )
             {
                memory[lno][0] = fill[0];
                memory[lno][1] = fill[1];
                memory[lno][2] = fill[2];
             }
          }
          else /* new label */
          {
             if ( label[lno] != NULL )
             {
               fprintf(stderr,"Internal error!\n");
             }
             label[lno] = calloc( sizeof(char), strlen($1)+1 );
             strcpy( label[lno], $1 );
          }
       }
     | hexno2 COLON
       {
          int i = hex2ui( $1[1] )*16 + hex2ui( $1[2] );

          if ( i < lno )
          {
             fprintf(stderr,"Label address %.2X too small!\n",i);
          }
          for ( ; lno<i; lno++ )
          {
             memory[lno][0] = fill[0];
             memory[lno][1] = fill[1];
             memory[lno][2] = fill[2];
          }
       }
     | opx NEWL
       {
          if ( lno > 255 )
          {
             fprintf(stderr,"Program too large!\n");
             lno = 255;
          }
          memory[lno][0] = $1[0];
          memory[lno][1] = $1[1];
          memory[lno][2] = $1[2];
          lno++;
       }
     ;


opx : NBR hexno3
      {   $<chr3>$[0] = $2[0];
          $<chr3>$[1] = $2[1];
          $<chr3>$[2] = $2[2];
      }
    | MOV hexno COMMA hexno
      {   $<chr3>$[0] = '0';
          $<chr3>$[1] = $2;
          $<chr3>$[2] = $4;
      }
    | MOVI hexno COMMA hexno
      {   $<chr3>$[0] = '1';
          $<chr3>$[1] = $2;
          $<chr3>$[2] = $4;
      }
    | AND hexno COMMA hexno
      {   $<chr3>$[0] = '2';
          $<chr3>$[1] = $2;
          $<chr3>$[2] = $4;
      }
    | ANDI hexno COMMA hexno
      {   $<chr3>$[0] = '3';
          $<chr3>$[1] = $2;
          $<chr3>$[2] = $4;
      }
    | ADD hexno COMMA hexno
      {   $<chr3>$[0] = '4';
          $<chr3>$[1] = $2;
          $<chr3>$[2] = $4;
      }
    | ADDI hexno COMMA hexno
      {   $<chr3>$[0] = '5';
          $<chr3>$[1] = $2;
          $<chr3>$[2] = $4;
      }
    | SUB hexno COMMA hexno
      {   $<chr3>$[0] = '6';
          $<chr3>$[1] = $2;
          $<chr3>$[2] = $4;
      }
    | SUBI hexno COMMA hexno
      {   $<chr3>$[0] = '7';
          $<chr3>$[1] = $2;
          $<chr3>$[2] = $4;
      }
    | CMP hexno COMMA hexno
      {   $<chr3>$[0] = '8';
          $<chr3>$[1] = $2;
          $<chr3>$[2] = $4;
      }
    | CMPI hexno COMMA hexno
      {   $<chr3>$[0] = '9';
          $<chr3>$[1] = $2;
          $<chr3>$[2] = $4;
      }
    | OR hexno COMMA hexno
      {   $<chr3>$[0] = 'A';
          $<chr3>$[1] = $2; 
          $<chr3>$[2] = $4;
      }
    | HALT
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0'; 
          $<chr3>$[2] = '0';
      }
    | NOP
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0';
          $<chr3>$[2] = '1';
      }
    | DISOUT
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0';
          $<chr3>$[2] = '2';
      }
    | HXDZ
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0';
          $<chr3>$[2] = '3';
      }
    | DZHX
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0';
          $<chr3>$[2] = '4';
      }
    | RND
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0';
          $<chr3>$[2] = '5';
      }
    | TIME
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0';
          $<chr3>$[2] = '6';
      }
    | RET
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0';
          $<chr3>$[2] = '7';
      }
    | CLEAR
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0';
          $<chr3>$[2] = '8';
      }
    | STC
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0';
          $<chr3>$[2] = '9';
      }
    | RSC
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0';
          $<chr3>$[2] = 'A';
      }
    | MULT
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0';
          $<chr3>$[2] = 'B';
      }
    | DIV
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0';
          $<chr3>$[2] = 'C';
      }
    | EXRL
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0';
          $<chr3>$[2] = 'D';
      }
    | EXRM
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0';
          $<chr3>$[2] = 'E';
      }
    | EXRA
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '0';
          $<chr3>$[2] = 'F';
      }
    | DISP char_1_6 COMMA hexno
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = $2;
          $<chr3>$[2] = $4;
      }
    | MAS hexno
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '7'; 
          $<chr3>$[2] = $2;
      }
    | INV hexno
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '8';
          $<chr3>$[2] = $2;
      }
    | SHR hexno
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = '9';
          $<chr3>$[2] = $2;  
      }
    | SHL hexno
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = 'A';
          $<chr3>$[2] = $2; 
      }
    | ADC hexno
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = 'B';
          $<chr3>$[2] = $2; 
      }
    | SUBC hexno
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = 'C';
          $<chr3>$[2] = $2; 
      }
    | DIN hexno
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = 'D';
          $<chr3>$[2] = $2; 
      }
    | DOT hexno
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = 'E';
          $<chr3>$[2] = $2; 
      }
    | KIN hexno
      {   $<chr3>$[0] = 'F';
          $<chr3>$[1] = 'F';
          $<chr3>$[2] = $2; 
      }
/* jumps */
    | CALL addr
      {   $<chr3>$[0] = 'B';
          $<chr3>$[1] = $2[1];
          $<chr3>$[2] = $2[2];
      }
    | GOTO addr
      {   $<chr3>$[0] = 'C';
          $<chr3>$[1] = $2[1];
          $<chr3>$[2] = $2[2];
      }
    | BRC addr
      {   $<chr3>$[0] = 'D';
          $<chr3>$[1] = $2[1];
          $<chr3>$[2] = $2[2];
      }
    | BRZ addr
      {   $<chr3>$[0] = 'E';
          $<chr3>$[1] = $2[1];
          $<chr3>$[2] = $2[2];
      }
    ;

addr : hexno2
       {   $<chr3>$[1] = $1[1];
           $<chr3>$[2] = $1[2];
       }
     | LABEL
       {
          int i;
          for ( i=0; i<256; i++ )
          {
             if ( label[i] )
             {
                if ( strstr(label[i], $1) && strstr( $1, label[i] ) ) break;
             }
          }

          if ( i < 256 ) /* label found */
          {
             $<chr3>$[1] = ui2hex( i/16 );
             $<chr3>$[2] = ui2hex( i%16 );
          }
          else /* new label */
          {
             if ( pre_label[lno] ) /* overfull */
             {
               fprintf(stderr,"Internal error!\n");
             }
             else
             {
                pre_label[lno] = calloc( sizeof(char), strlen($1)+1 );
                strcpy( pre_label[lno], $1 );
             }
             $<chr3>$[1] = 'X';
             $<chr3>$[2] = 'Y';
          }
       }
     ;

hexno3: hexno hexno hexno
        {   $$[0] = $1;
            $$[1] = $2;
            $$[2] = $3;
        }
        ;

hexno2: hexno hexno
        {   $$[1] = $1;
            $$[2] = $2;
        }
        ;

hexno : CHAR_0   { $$ = $1; }
      | char_1_6 { $$ = $1; }
      | CHAR_7_F { $$ = $1; }
      ;

char_1_6 : CHAR_1_6 { $$ = $1; }
         ;


%%


#include "micasmL.lex.c"


int yyerror(char *s)
{
   error_line = yylineno;
   error_msg  = s;
   return(0);
}

int yywrap()
{
   error_line = 0;
   error_msg  = "";
   return(1);
}
