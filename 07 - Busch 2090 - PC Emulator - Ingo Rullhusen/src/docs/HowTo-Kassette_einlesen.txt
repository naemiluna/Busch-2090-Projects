
   Kassetten in MIC-Dateien konvertieren
   =====================================

Um die Programme von einer Aufnahme des Cassetten-Interface 2095 in
MIC-Dateien zu extrahieren, bedarf es der folgenden Schritte.

1.) Aufnahme

Abspielen der Musikkassette auf einem Stereo-Kasettendeck mit richtiger
Bandartenwahl aber ohne Rauschunterdr�ckung oder sonstige
Signalver�nderungen. Aufnahme bei korrekter Aussteuerung �ber eine Soundkarte
(z.B. mit krecord) als WAV-Datei (44100 Hz, stereo, 16 Bit).

2.) Schnitt

Die aufgenommene WAV-Datei in einen Audioeditor (z.B. audacity) laden. Die
schlechtere Stereokanal-Spur (Rauschen/Signal) ist zu l�schen, die bessere
auf mono zu setzen. Die Aufnahme ist zu schneiden. Das Ergebnis ist als
direktes Abbild der Kassette zu speichern.

Zur Korrektur der Bandgeschwindigkeit ist jetzt noch die Frequenz der
Initialsequenz zu bestimmen (Spektrum), die bei etwa 2272 Hz liegen sollte.
Der Korrekturfaktor f�r die Bandgeschwindigkeit ergibt sich dann zu:

 2272 / gemessene_Initialfrequenz_in_Hz

3.) Fehlerkorrektur

Die niedrigste Frequenz des Cassetten-Interface 2095 liegt bei etwa 568 Hz.
Somit kann mit einem Hochpa� bei einer Grenzfrequenz von 500 Hz der
niederfrequente St�ranteil eliminiert werden, ohne Information zu verlieren.
Mit dem Programm sox ergibt das beispielsweise zusammen mit einer
Bandgeschwindigkeitskorrektur von 1,05 den Aufruf:

 sox micprog1-1.wav ds1_s.wav speed 1.05 highpass 500

4.) Signalformung

Um ein Rechtecksignal zur�ckzugewinnen wird nun maximal verst�rkt.
Gleichzeitig kann mit sox jetzt auch in die 8-Bit-Aufl�sung konvertiert
werden. Der entsprechende Aufruf lautet beispielsweise:

 sox ds1_s.wav -v1000.0 -b ds1_b.wav

5.) Konvertierung

Aus der derart aufbereiteten WAV-Datei k�nnen nun die Microtronic-Programme
als MIC-Dateien extrahiert werden:

 wav2mic ds1_b.wav

Da das Kassetten-Dateiformat keinerlei Pr�fsumme enth�lt, kann die Integrit�t
der extrahierten Daten nicht garantiert werden. Ein exemplarischer Vergleich
einiger Programme mit ihrem Listing w�re hilfreich. Kleinste Ver�nderungen
der Bandgeschwindigkeitskorrektur k�nnen ggf. Wunder bewirken.
