//
// stdin_thread.h
// Copyright (C) 8.10.2005 Ingo D. Rullhusen <d01c@uni-bremen.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//

#ifndef STDIN_THREAD_H
#define STDIN_THREAD_H

#include <termios.h>

#include <qthread.h>

class STDIN_thread : public QThread
{
   public:
      bool go_on;

      STDIN_thread();
      ~STDIN_thread();

      virtual void run();

   private:
      static const int fd = 0; /* stdin */
      struct termios term, oterm;
      long ofstatus;
};

#endif
