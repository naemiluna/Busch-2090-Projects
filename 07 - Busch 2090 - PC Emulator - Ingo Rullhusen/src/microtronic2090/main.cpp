//
// main.h
#define VERSION "22.10.2005"
#define CPR "(c) 2005 Dr.-Ing. Ingo D. Rullhusen <d01c@uni-bremen.de>"
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//

#include <iostream>

#include <qapplication.h>

#include "microtronic.h"
#include "MIC2090.h"
#include "MICclock.h"
#include "stdin_thread.h"

using namespace std;


QApplication *pA;
mainform *pW;

core2090 MIC;
clock2090 CLK;
STDIN_thread CIN;


int main( int argc, char ** argv )
{
   QApplication a( argc, argv );
   pA = &a;
   mainform W;
   pW = &W;

   cerr << "microtronic (version " << VERSION << ")" << endl;
   cerr << " Copyright " << CPR << endl;
   cerr << " microtronic comes with ABSOLUTELY NO WARRANTY; for details see GNU" << endl;
   cerr << " GENERAL PUBLIC LICENSE. This is free software, and you are welcome to" << endl;
   cerr << " redistribute it under certain conditions; see GNU GENERAL PUBLIC LICENSE" << endl;
   cerr << " for details." << endl << endl;

   CLK.start();
   MIC.start();

   W.show();
   a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
   a.exec();

   CIN.go_on = false;
   CIN.wait( 1000 );

   MIC.wait( 1000 );
   CLK.wait( 1000 );

   return(0);
}
