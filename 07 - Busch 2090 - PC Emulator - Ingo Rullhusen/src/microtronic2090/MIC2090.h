//
// MIC2090.h
// Copyright (C) 10.9.2005 Ingo D. Rullhusen <d01c@uni-bremen.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//

#ifndef MIC2090_H
#define MIC2090_H

#include <qthread.h>
#include <qstring.h>


typedef enum
{
   KEY_0 = 0,
          KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7,
   KEY_8, KEY_9, KEY_A, KEY_B, KEY_C, KEY_D, KEY_E, KEY_F,

   NEXT, REG,
   BKP,  STEP,
   RUN,  HALT,
   CCE,  PGM,

   RESET, TSTART,

   NONE
} Keys2090;


class core2090 : public QThread
{
   public:
      Keys2090 keystroke;
      bool in[4], out[4];
      bool carry, zero;
      unsigned char display[6];

      bool max_speed, register_dump;

      QString filename;
      bool lock;

      bool go_on;

      core2090();

      virtual void run();

      void reset();

   private:
      unsigned char memory[256][3];
      unsigned char addr[2], call_addr[2], bkp[2], reg[16], sreg[16];
      unsigned char disp_no, disp_reg;
      bool disout, brk;
      unsigned char last_out;

      void UpdateW( bool flush = true );

      void get_line();
      void get_regs();
      void get_dummy();

      void alu( bool stepmode );

      void load_pgm();
      void save_pgm();

      void f_next();
      void f_step();
      void f_run();
      void f_pgm();
      void f_bkp();
      unsigned char f_reg();

      void reg_dump();
};

#endif
