//
// MIC2090.cpp
// Copyright (C) 22.10.2005 Ingo D. Rullhusen <d01c@uni-bremen.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

#include <qapplication.h>
#include <qdatetime.h>
#include <qstring.h>
#include <qcheckbox.h>
#include <qlcdnumber.h>

#include "microtronic.h"
#include "Nimm-Spiel.h"
#include "convert.h"
#include "Combos.h"
#include "MyEvents.h"
#include "MIC2090.h"
#include "MICclock.h"

using namespace std;

extern QApplication *pA;
extern mainform *pW;
extern clock2090 CLK;


#define SLEEPTIME 1000


void core2090::UpdateW( bool flush )
{
   if ( ! go_on ) exit();
   pA->lock();

   pW->out1->setChecked( out[0] );
   pW->out2->setChecked( out[1] );
   pW->out3->setChecked( out[2] );
   pW->out4->setChecked( out[3] );
   if ( pW->OutCon->currentItem() == STDOUT )
   {
      unsigned char next_out;
      next_out = 0;
      if ( out[0] ) next_out |= 0x1;
      if ( out[1] ) next_out |= 0x2;
      if ( out[2] ) next_out |= 0x4;
      if ( out[3] ) next_out |= 0x8;

      if ( next_out != last_out && go_on )
      {
         last_out = next_out;
         cout << ui2hex(next_out) << endl;
      }
   }

   QString s;
   s = ui2hex( display[0] );
   pW->digit1->display( s );
   s = ui2hex( display[1] );
   pW->digit2->display( s );
   s = ui2hex( display[2] );
   pW->digit3->display( s );
   s = ui2hex( display[3] );
   pW->digit4->display( s );
   s = ui2hex( display[4] );
   pW->digit5->display( s );
   s = ui2hex( display[5] );
   pW->digit6->display( s );

   pW->carry->setChecked( carry );
   pW->zero->setChecked( zero );

   pA->unlock();

   if ( flush )
   {
      QCustomEvent *e = new QCustomEvent(UPDATE_EVENT);
      QApplication::postEvent( pW, e );   // *e deleted by postEvent
   }
}


core2090::core2090() : filename("")
{
   for ( int i=0; i<256; i++ )
      memory[i][0] = memory[i][1] = memory[i][2] = 0xf;
}


void core2090::reg_dump()
{
   int a;

   cerr.clear();

   a = 16*addr[1] + addr[0];
   if ( a > 255 )
   {
      cerr << "Internal error: core2090 line "
           << __LINE__ << "!" << endl;
   }

   cerr << ">" << ui2hex( addr[1] )
               << ui2hex( addr[0] ) << ":"
        << ui2hex( memory[a][0] )
        << ui2hex( memory[a][1] )
        << ui2hex( memory[a][2] ) << ":";

   if ( carry ) cerr << "c";
   else         cerr << ".";

   if ( zero ) cerr << "z";
   else        cerr << ".";

   cerr << ":";

   for ( int i=0; i<16; i++ ) cerr << ui2hex( reg[i] );
   cerr << ":";
   for ( int i=0; i<16; i++ ) cerr << ui2hex( sreg[i] );

   cerr << endl;
}


void core2090::reset()
{
   keystroke = NONE;
   out[0] = out[1] = out[2] = out[3] = false;
   last_out = 0xff;
   carry = zero = false;

   get_dummy();

// memory not deleted!

   addr[0] = addr[1] = 0;
   call_addr[0] = call_addr[1] = 0;
   bkp[0] = bkp[1] = 0;

   disp_no = disp_reg = 0;
   disout = false;

   brk = false;

   for ( int i=0; i<16; i++ )
   {
      reg[i] = sreg[i] = 0;
   }
   reg[15] = 0xf;
}


void core2090::f_next()
{
   unsigned char r;
   unsigned int a, dig;
   Keys2090 last_key;

   dig = 0;
   last_key = NONE;

// set address
   while ( dig != 2 )
   {
      usleep(SLEEPTIME);

      switch (keystroke)
      {
         case CCE:
         {
//            addr[0] = addr[1];
//            addr[1] = 0x0;
            dig = 0;
            break;
         }

         case KEY_0:
         case KEY_1:
         case KEY_2:
         case KEY_3:
         case KEY_4:
         case KEY_5:
         case KEY_6:
         case KEY_7:
         case KEY_8:
         case KEY_9:
         case KEY_A:
         case KEY_B:
         case KEY_C:
         case KEY_D:
         case KEY_E:
         case KEY_F:
         {
            if ( dig > 0 )
               addr[1] = addr[0];
            else
               addr[1] = 0x0;

            addr[0] = keystroke;
            dig++;
            break;
         }

         case REG:
         {
            if ( (r = f_reg()) < 16 )
            {
               addr[0] = r;
               addr[1] = 0x0;
            }
            dig = 2;
         }

         case PGM:
         case HALT: { return; }

         default: {}
      }
      if ( keystroke != NONE )
      {
         display[0] = addr[1];
         display[1] = addr[0];
         UpdateW();
         keystroke = NONE;
      }
   }

// change memory
   get_line();
   UpdateW();
   display[3] = 0x0;
   display[4] = 0x0;
   display[5] = 0x0;

   a = 16*addr[1] + addr[0];
   if ( a > 255 )
   {
      cerr << "Internal error: core2090 line "
           << __LINE__ << "!" << endl;
      reset();
      keystroke = HALT;
   }

   while ( keystroke != HALT )
   {
      usleep(SLEEPTIME);

      switch (keystroke)
      {
         case CCE:
         {
            if ( last_key == CCE )
            {
               display[3] = 0x0;
               display[4] = 0x0;
               display[5] = 0x0; 
            }
            else
            {
               display[5] = display[4];
               display[4] = display[3];
               display[3] = 0x0;
            }

            last_key = CCE;
            UpdateW();

            break;
         }

         case KEY_0:
         case KEY_1:
         case KEY_2:
         case KEY_3:
         case KEY_4:
         case KEY_5:
         case KEY_6:
         case KEY_7:
         case KEY_8:
         case KEY_9:
         case KEY_A:
         case KEY_B:
         case KEY_C:
         case KEY_D:
         case KEY_E:
         case KEY_F:
         {
            last_key = keystroke;

            display[3] = display[4];
            display[4] = display[5];
            display[5] = keystroke;

            UpdateW();

            break;
         }

         case NEXT:
         {
            if ( last_key != NEXT && last_key != NONE )
            {
               memory[a][0] = display[3];
               memory[a][1] = display[4];
               memory[a][2] = display[5];

               r = ~ display[5];
               out[0] = r & 0x1;
               out[1] = r & 0x2;
               out[2] = r & 0x4;
               out[3] = r & 0x8;
            }

            last_key = keystroke;

            a++;
            if ( a > 255 ) a = 0;

            addr[0] = a % 16;
            addr[1] = a >> 4;

            get_line();
            UpdateW();
            display[3] = 0x0;
            display[4] = 0x0;
            display[5] = 0x0;

            break;
         }

         case REG:
         {
            if ( (r = f_reg()) < 16 )
            {
               get_line();
               UpdateW();
               display[3] = 0x0;
               display[4] = 0x0;
               display[5] = r;
            }
            else
            {
               return;
            }
            break;
         }

         case PGM:
         case RUN:
         case BKP:
         case STEP:
         case HALT: { return; }

         default: {}
      }

      keystroke = NONE;
   }

}


unsigned char core2090::f_reg()
{
   unsigned char r;

// get register
   r = 0xff;
   while ( r > 15 )
   {
      usleep(SLEEPTIME);

      switch ( keystroke )
      {
         case KEY_0:
         case KEY_1:
         case KEY_2:
         case KEY_3:
         case KEY_4:
         case KEY_5:
         case KEY_6:
         case KEY_7:
         case KEY_8:
         case KEY_9:
         case KEY_A:
         case KEY_B:
         case KEY_C:
         case KEY_D:
         case KEY_E:
         case KEY_F:
         {
            r = keystroke;
            break;
         }

         case HALT: { return(r); }

         default: {};
      }

      keystroke = NONE;

   }

// change register
   display[0] = 0xff;
   display[1] = 0xff;
   display[2] = r;
   display[3] = 0xff;
   display[4] = 0xff;
   display[5] = reg[r];
   UpdateW();

   while ( 1 )
   {
      usleep(SLEEPTIME);

      switch ( keystroke )
      {
         case KEY_0:
         case KEY_1:
         case KEY_2:
         case KEY_3:
         case KEY_4:
         case KEY_5:
         case KEY_6:
         case KEY_7:
         case KEY_8:
         case KEY_9:
         case KEY_A:
         case KEY_B:
         case KEY_C:
         case KEY_D:
         case KEY_E:
         case KEY_F:
         {
            reg[r] = keystroke;
            display[5] = reg[r];
            UpdateW();
            break;
         }

         case HALT: { return(r); }

         default: {};
      }

      keystroke = NONE;

   }

   return(r);
}


void core2090::f_pgm()
{

   while ( go_on )
   {
      usleep(SLEEPTIME);

      switch ( keystroke )
      {
         case KEY_0: // selfcheck
         {
// part 1
            out[0] = false;
            out[1] = false;
            out[2] = false;
            out[3] = false;

            zero = false;
            keystroke = NONE;

            for ( int i=0; i<16; i++ )
            {
               display[0] = display[1] = display[2]
                          = display[3] = display[4]
                          = display[5] = i;
               msleep(500);
               UpdateW();
            }

            carry = zero = true;

// part 2
            bool wait;

            for ( int i=0; i<16; i++ )
            {
               display[0] = display[1] = 0x0;
               display[2] = 0xff;
               display[3] = 0x1;
               display[4] = 0x0;
               display[5] = i;
               UpdateW();

               wait = true;

               while (wait)
               {
                  switch (keystroke)
                  {
                     case KEY_0:
                     case KEY_1:
                     case KEY_2:
                     case KEY_3:
                     case KEY_4:
                     case KEY_5:
                     case KEY_6:
                     case KEY_7:
                     case KEY_8:
                     case KEY_9:
                     case KEY_A:
                     case KEY_B:
                     case KEY_C:
                     case KEY_D:
                     case KEY_E:
                     case KEY_F:
                     {
                        if ( keystroke == i )
                        {
                           wait = false;
                        }
                        else
                        {
                           display[0] = 0xf;
                           display[1] = 0xe;
                           display[2] = 0xff;
                           display[3] = 0x1;
                           display[4] = keystroke;
                           display[5] = i;
                           UpdateW();
                           keystroke = NONE;
                        }
                        break;
                     }

                     case HALT:
                     {
                        carry = zero = false;
                        return;
                     }

                     default: {}
                  }
                  keystroke = NONE;
                  usleep(SLEEPTIME);
               }
            }

// part 3
            carry = zero = false;

            for ( int i=0; i<16; i++ )
            {
               display[0] = display[1] = 0x0;
               display[2] = 0xff;
               display[3] = 0x2;
               display[4] = i;
               display[5] = i;

               out[0] = i & 0x1;
               out[1] = i & 0x2;
               out[2] = i & 0x4;
               out[3] = i & 0x8;

               UpdateW();

               if ( in[0] != out[0] ||
                    in[1] != out[1] ||
                    in[2] != out[2] ||
                    in[3] != out[3]    )
               {
                  display[0] = 0xf;
                  display[1] = 0xe;
                  display[2] = 0xff;
                  display[3] = 0x2;
                  display[4] = i;
                  display[5] = i;

                  carry = zero = true;

                  UpdateW();

                  while ( keystroke != HALT ){ usleep(SLEEPTIME); }

                  carry = zero = false;

                  return;
               }

               msleep(500);
            }

// part 4
            zero = true;
            display[0] = display[1] = display[2]
                       = display[3] = display[4]
                       = display[5] = 0xff;
            UpdateW();

            msleep(1500);

            for ( int i=0; i<4; i++ )
            {
               out[0] = true;
               out[1] = true;
               out[2] = true;
               out[3] = true;
               out[i] = false;

               UpdateW();
               msleep(1500);
            }

            out[3] = true;

            UpdateW();

            msleep(500);

            reset();

            out[0] = out[1] = out[2] = out[3] = true;

            for ( int i=0; i<256; i++ )
               memory[i][0] = memory[i][1] = memory[i][2] = 0;

            return;
         }
         case KEY_1: // load from cassette
         {
            lock = true;
            QCustomEvent *e = new QCustomEvent(LOAD_EVENT);
            QApplication::postEvent( pW, e );   // *e deleted by postEvent
            while ( lock ){ usleep(SLEEPTIME); }

            load_pgm();

            return;
         }
         case KEY_2: // save to cassette
         {
            lock = true;
            QCustomEvent *e = new QCustomEvent(SAVE_EVENT);
            QApplication::postEvent( pW, e );   // *e deleted by postEvent
            while ( lock ){ usleep(SLEEPTIME); }

            save_pgm();
         
            return;
         }
         case KEY_3: // set time
         {
            unsigned char ct;

            CLK.time_val.lock();
            display[0] = CLK.h2;
            display[1] = CLK.h1;
            display[2] = CLK.m2;
            display[3] = ct = CLK.m1;
            display[4] = CLK.s2;
            display[5] = CLK.s1;
            CLK.time_val.unlock();

            keystroke = NONE;
            while ( keystroke != HALT )
            {
               switch( keystroke )
               {
                  case KEY_0:
                  case KEY_1:
                  case KEY_2:
                  case KEY_3:
                  case KEY_4:
                  case KEY_5:
                  case KEY_6:
                  case KEY_7:
                  case KEY_8:
                  case KEY_9:
                  case KEY_A:
                  case KEY_B:
                  case KEY_C:
                  case KEY_D:
                  case KEY_E:
                  case KEY_F:
                  {
                     display[0] = display[1];
                     display[1] = display[2];
                     display[2] = ct;
                     display[3] = ct = keystroke;
                     display[4] = 0x0;
                     display[5] = 0x0;

                     CLK.time_val.lock();
                     CLK.h2 = display[0];
                     CLK.h1 = display[1];
                     CLK.m2 = display[2];
                     CLK.m1 = display[3];
                     CLK.s2 = display[4];
                     CLK.s1 = display[5];
                     CLK.time_val.unlock();

                     keystroke = NONE;
                     break;
                  }

                  case CCE:
                  {
                     display[3] = 0x0;

                     keystroke = NONE;
                     break;
                  }

                  case STEP:
                  case RUN: break;

                  default: {};
               }

               UpdateW();
               usleep(SLEEPTIME);
            }

            return;
         }
         case KEY_4: // show time
         {
            keystroke = NONE;
            while ( keystroke != HALT )
            {
               CLK.time_val.lock();

               display[0] = CLK.h2;
               display[1] = CLK.h1;
               display[2] = CLK.m2;
               display[3] = CLK.m1;
               display[4] = CLK.s2;
               display[5] = CLK.s1;

               CLK.s1 %= 10;
               out[0] = CLK.s1 & 0x01;
               out[1] = CLK.s1 & 0x02;
               out[2] = CLK.s1 & 0x04;
               out[3] = CLK.s1 & 0x08;

               carry = ! ( CLK.s1 % 2 );

               CLK.time_val.unlock();

               UpdateW();
               usleep(SLEEPTIME);
            }

            carry = false;
            return;
         }
         case KEY_5: // delete
         {
            for ( int i=0; i<256; i++ )
            {
               memory[i][0] = 0x00;
               memory[i][1] = 0x00;
               memory[i][2] = 0x00;
            }
            out[0] = out[1] = out[2] = out[3] = true;
            addr[0] = addr[1] = 0;
            bkp[0] = bkp[1] = 0;
// register & flags kept

            return;
         }
         case KEY_6: // all NOP
         {
            for ( int i=0; i<256; i++ )
            {
               memory[i][0] = 0x0f;
               memory[i][1] = 0x00;
               memory[i][2] = 0x01;
            }
            out[0] = false;
            out[1] = out[2] = out[3] = true;
            addr[0] = addr[1] = 0;
            bkp[0] = bkp[1] = 0;
// register & flags kept

            return;
         }
         case KEY_7: // Nimm-Spiel
         {
            for ( int i=0; i<nimm_size; i++ )
            {
               memory[i][0] = NimmSpiel[i][0];
               memory[i][1] = NimmSpiel[i][1];
               memory[i][2] = NimmSpiel[i][2];
            }
            addr[0] = (nimm_size-1) % 16;
            addr[1] = (nimm_size-1) / 16;

            unsigned char r = ~ memory[ nimm_size-1 ][2];
            out[0] = r & 0x1;
            out[1] = r & 0x2;
            out[2] = r & 0x4;
            out[3] = r & 0x8;

            return;
         }


         case KEY_F: // EXTENSION: get system time
         {
            unsigned char ui;
            QTime t;

            CLK.time_val.lock();

            t.start();
            ui = t.hour();
            CLK.h2 = ui / 10;
            CLK.h1 = ui % 10;
            ui = t.minute();
            CLK.m2 = ui / 10;
            CLK.m1 = ui % 10;
            ui = t.second();
            CLK.s2 = ui / 10;
            CLK.s1 = ui % 10;

            CLK.time_val.unlock();
            return;
         }


         case HALT: { return; }

         default: {};
      }

   }

}


void core2090::load_pgm()
{
   char line[200];
   fstream MicFile;

   MicFile.open( filename, ios::in );
   if ( ! MicFile.is_open() ) return;

   int i=0;

   // 9 July 2017, Martin Sauter
   // Program load routine changed to allow input files
   // with empty lines
   while (MicFile.getline(line,200))
   {

      if (strlen(line) > 2) {
         memory[i][0] = hex2ui( line[0] );
         memory[i][1] = hex2ui( line[1] );
         memory[i][2] = hex2ui( line[2] );

         cout << i << " " << line << "\r\n";
         i++;
      }
   }

   reset();

   addr[0] = 0xf;
   addr[1] = 0xf;

   MicFile.close();
}

void core2090::save_pgm()
{
   fstream MicFile;

   MicFile.open( filename, ios::out );
   if ( ! MicFile.is_open() ) return;

   for ( int i=0; i<256; i++ )
   {
      MicFile << ui2hex( memory[i][0] );
      MicFile << ui2hex( memory[i][1] );
      MicFile << ui2hex( memory[i][2] );
      MicFile << '\r';
   }

   MicFile.close();
}


void core2090::alu( bool stepmode )
{
   char c1, c2, c3;
   unsigned char opi1, opi2, acc1, acc2;
   unsigned long int i1, i2;
   int a;

   a = 16*addr[1] + addr[0];
   if ( a > 255 )
   {
      cerr << "Internal error: core2090 line "
           << __LINE__ << "!" << endl;
      reset();
      keystroke = HALT;
      return;
   }
   c1 = ui2hex( memory[a][0] );
   c2 = ui2hex( opi1 = memory[a][1] );
   c3 = ui2hex( opi2 = memory[a][2] );

   a++;
   if ( a > 255 ) a = 0;

   switch( c1 )
   {
      case '0': /* MOV */
      {
         reg[ opi2 ] = reg[ opi1 ];
         zero = ( reg[ opi2 ] == 0 );

         break;
      }
      case '1': /* MOVI */
      {
         reg[ opi2 ] = opi1;
         zero = ( opi1 == 0 );

         break;
      }
      case '2': /* AND */
      {
         acc1 = reg[ opi1 ] & reg[ opi2 ];
         reg[ opi2 ] = acc1 & 0x0f;
         carry = false;
         zero = ( reg[ opi2 ] == 0 );

         break;
      }
      case '3': /* ANDI */
      {
         acc1 = opi1 & reg[ opi2 ];
         reg[ opi2 ] = acc1 & 0x0f;
         carry = false;
         zero = ( reg[ opi2 ] == 0 );

         break;
      }
      case '4': /* ADD */
      {
         acc1 = reg[ opi1 ] + reg[ opi2 ];
         reg[ opi2 ] = acc1 & 0x0f;
         carry = acc1 & 0xf0;
         zero = ( reg[ opi2 ] == 0 );

         break;
      }
      case '5': /* ADDI */
      {
         acc1 = opi1 + reg[ opi2 ];
         reg[ opi2 ] = acc1 & 0x0f;
         carry = acc1 & 0xf0;
         zero = ( reg[ opi2 ] == 0 );

         break;
      }
      case '6': /* SUB */
      {
         acc1 = reg[ opi2 ] - reg[ opi1 ];
         reg[ opi2 ] = acc1 & 0x0f;
         carry = acc1 & 0xf0;
         zero = ( reg[ opi2 ] == 0 );

         break;
      }
      case '7': /* SUBI */
      {
         acc1 = reg[ opi2 ] - opi1;
         reg[ opi2 ] = acc1 & 0x0f;
         carry = acc1 & 0xf0;
         zero = ( reg[ opi2 ] == 0 );

         break;
      }
      case '8': /* CMP */
      {
         acc1 = reg[ opi1 ];
         acc2 = reg[ opi2 ];
         carry = ( acc1 < acc2 );
         zero  = ( acc1 == acc2 );

         break;
      }
      case '9': /* CMPI */
      {
         acc1 = opi1;
         acc2 = reg[ opi2 ];
         carry = ( acc1 < acc2 );
         zero  = ( acc1 == acc2 );

         break;
      }
      case 'A': /* OR */
      {
         acc1 = reg[ opi1 ] | reg[ opi2 ];
         reg[ opi2 ] = acc1 & 0x0f;
         carry = false;
         zero = ( reg[ opi2 ] == 0 );

         break;
      }
      case 'B': /* CALL */
      {
         call_addr[0] = addr[0];
         call_addr[1] = addr[1];

         a = 16*opi1 + opi2;

         break;
      }
      case 'C': /* GOTO */
      {
         a = 16*opi1 + opi2;

         break;
      }
      case 'D': /* BRC */
      {
         if ( carry ) a = 16*opi1 + opi2;
         else if ( stepmode )
         {
            addr[0] = a % 16;
            addr[1] = a >> 4;
            if ( register_dump ) reg_dump();
            alu( true );
            a = 16*addr[1] + addr[0];
         }

         break;
      }
      case 'E': /* BRZ */
      {
         if ( zero ) a = 16*opi1 + opi2;
         else if ( stepmode )
         {
            addr[0] = a % 16;
            addr[1] = a >> 4;
            if ( register_dump ) reg_dump();
            alu( true );
            a = 16*addr[1] + addr[0];
         }

         break;
      }

      case 'F':
      {
         switch(c2)
         {
            case '0':
            {
               switch (c3)
               {
                  case '0': /* HALT */
                  {
                     brk = true;
                     if ( ! stepmode ) a = 16*addr[1] + addr[0];

                     break;
                  }
                  case '1': /* NOP */
                  {
                     break;
                  }
                  case '2': /* DISOUT */
                  {
                     disout = true;

                     display[0] = display[1] = display[2] = display[3]
                                = display[4] = display[5] = 0xff;

                     break;
                  }
                  case '3': /* HXDZ */
                  {
                     i1 = 16*(16*reg[15] + reg[14]) + reg[13];
                     carry = false;
                     if ( i1 > 999 )
                     {
                        reg[15] = reg[14] = reg[13] = 0;
                        zero = true;
                     }
                     else
                     {
                        reg[13] = i1%10;
                        i1 /= 10;
                        reg[14] = i1%10;
                        i1 /= 10;
                        reg[15] = i1;
                        zero = false;
                     }

                     break;
                  }
                  case '4': /* DZHX */
                  {
                     i1 = 10*(10*reg[15] + reg[14]) + reg[13];
                     reg[13] = 0xf & i1;
                     i1 = i1 >> 4;
                     reg[14] = 0xf & i1;
                     i1 = i1 >> 4;
                     reg[15] = 0xf & i1;

                     carry = false;
                     zero = false;

                     break;
                  }
                  case '5': /* RND */
                  {
                     struct timeval tv;
                     gettimeofday(&tv, NULL);
                     i1 = tv.tv_sec*(long)1000 + tv.tv_usec/(long)1000;
                     i1 >>= 2;
                  
                     reg[13] = 0xf & i1;
                     i1 = i1 >> 4;
                     reg[14] = 0xf & i1;
                     i1 = i1 >> 4;
                     reg[15] = 0xf & i1;

                     break;
                  }
                  case '6': /* TIME */
                  {
                     CLK.time_val.lock();

                     reg[10] = CLK.s1;
                     reg[11] = CLK.s2;
                     reg[12] = CLK.m1;
                     reg[13] = CLK.m2;
                     reg[14] = CLK.h1;
                     reg[15] = CLK.h2;

                     CLK.time_val.unlock();

                     break;
                  }
                  case '7': /* RET */
                  {
                     a = 16*call_addr[1] + call_addr[0];
                     a++;

                     break;
                  }
                  case '8': /* CLEAR */
                  {
                     for ( int r=0; r<16; r++ ) reg[r] = 0;

                     carry = false;
                     zero  = true;

                     break;
                  }
                  case '9': /* STC */
                  {
                     carry = true;

                     break;
                  }
                  case 'A': /* RSC */
                  {
                     carry = false;

                     break;
                  }
                  case 'B': /* MULT */
                  {
                     i1 = 10*(10*(10*(10*(10*reg[5] + reg[4]) + reg[3]) + reg[2]) + reg[1]) + reg[0];
                     i2 = 10*(10*(10*(10*(10*sreg[5] + sreg[4]) + sreg[3]) + sreg[2]) + sreg[1]) + sreg[0];

                     if ( reg[0] > 0x9 || reg[1] > 0x9 ||
                          reg[2] > 0x9 || reg[3] > 0x9 ||
                          reg[4] > 0x9 || reg[5] > 0x9 ||
                          sreg[0] > 0x9 || sreg[1] > 0x9 ||
                          sreg[2] > 0x9 || sreg[3] > 0x9 ||
                          sreg[4] > 0x9 || sreg[5] > 0x9    )
                     {
                        carry = true;
                     }
                     else if ( (double)i1 * (double)i2 > 999999.0 )
                     {
                        reg[5] = reg[4] = reg[3]
                               = reg[2] = reg[1] = reg[0] = 0xe;

                        carry = true;
                     }
                     else
                     {
                        i1 *= i2;

                        reg[0] = i1%10;
                        i1 /= 10;
                        reg[1] = i1%10;
                        i1 /= 10;
                        reg[2] = i1%10;
                        i1 /= 10;
                        reg[3] = i1%10;
                        i1 /= 10;
                        reg[4] = i1%10;
                        i1 /= 10;
                        reg[5] = i1%10;

                        for ( int i=0; i<6; i++ ) sreg[i] = 0;
                        carry = false;
                     }

                     zero = false;

                     break;
                  }
                  case 'C': /* DIV */
                  {
                     unsigned long int i3;

                     i1 = 10*(10*(10*reg[3]  + reg[2])  + reg[1])  + reg[0];
                     i2 = 10*(10*(10*sreg[3] + sreg[2]) + sreg[1]) + sreg[0];

                     if ( reg[0] > 0x9  || reg[1] > 0x9  || reg[2] > 0x9  ||
                          reg[3] > 0x9  || reg[4] > 0x9  || reg[5] > 0x9  ||
                          sreg[0] > 0x9 || sreg[1] > 0x9 || sreg[2] > 0x9 ||
                          sreg[3] > 0x9 || sreg[4] > 0x9 || sreg[5] > 0x9    )
                     {
                        carry = true;
                        zero  = false;
                     }
                     else if ( i1 == 0 || i2 == 0 )
                     {
                        reg[5] = reg[4] = reg[3] = reg[2] = reg[1] = reg[0] = 0xe;
                        carry = true;
                        zero  = false;
                     }
                     else
                     {
                        if ( ! max_speed )
                        {
                           display[0] = display[1] = display[2]
                                      = display[3] = display[4]
                                      = display[5] = 0xff;
                           UpdateW();
                           i3 = 6000*(i2/i1)/9999;
                           if ( i3 > 10 ) msleep( (unsigned long)i3 );
                        }

                        i3 = i2 % i1;
                        i1 = i2 / i1;

                        reg[0] = i1%10;
                        i1 /= 10;
                        reg[1] = i1%10;
                        i1 /= 10;
                        reg[2] = i1%10;
                        i1 /= 10;
                        reg[3] = i1%10;
                        i1 /= 10;
                        reg[4] = i1%10;
                        i1 /= 10;
                        reg[5] = i1%10;

                        zero = ( i3 > 0 );

                        sreg[0] = i3%10;
                        i3 /= 10;
                        sreg[1] = i3%10;
                        i3 /= 10;
                        sreg[2] = i3%10;
                        i3 /= 10;
                        sreg[3] = i3%10;
                        i3 /= 10;
                        sreg[4] = i3%10;
                        i3 /= 10;
                        sreg[5] = i3%10;

                        carry = false;
                     }

                     break;
                  }
                  case 'D': /* EXRL */
                  {
                     for ( int r=0; r<8; r++ )
                     {
                        acc1 = reg[r];
                        reg[r] = sreg[r];
                        sreg[r] = acc1;
                     }

                     break;
                  }
                  case 'E': /* EXRM */
                  {
                     for ( int r=8; r<16; r++ )
                     {
                        acc1 = reg[r];
                        reg[r] = sreg[r];
                        sreg[r] = acc1;
                     }

                     break;
                  }
                  case 'F': /* EXRA */
                  {
                     for ( int r=0; r<8; r++ )
                     {
                        acc1 = reg[r];
                        reg[r] = reg[r+8];
                        reg[r+8] = acc1;
                     }

                     break;
                  }

                  default:
                  {
                     cerr << "Internal error: core2090 line "
                          << __LINE__ << "!" << endl;
                  }
               }

               break;
            }

            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6': /* DISP */
            {
               disout = false;
               disp_no  = opi1;
               disp_reg = opi2;

               break;
            }

            case '7': /* MAS */
            {
               sreg[ opi2 ] = reg[ opi2 ];

               break;
            }
            case '8': /* INV */
            {
               acc1 = reg[ opi2 ];
               acc1 = 0xf & ( ~ acc1 );
               reg[ opi2 ] = acc1;

               carry = false;
               zero  = ( acc1 == 0 );

               break;
            }
            case '9': /* SHR */
            {
               acc1 = reg[ opi2 ];
               carry = 0x1 & acc1;
               acc1 >>= 1;
               acc1 = 0xf & acc1;
               reg[ opi2 ] = acc1;

               zero  = ( acc1 == 0 );

               break;
            }
            case 'A': /* SHL */
            {
               acc1 = reg[ opi2 ];
               carry = 0x8 & acc1;
               acc1 <<= 1;
               acc1 = 0xf & acc1;
               reg[ opi2 ] = acc1;

               zero  = ( acc1 == 0 );

               break;
            }
            case 'B': /* ADC */
            {
               acc1 = reg[ opi2 ];
               if ( carry ) acc1++;

               carry = ( acc1 > 15 );
               acc1 = 0xf & acc1;
               reg[ opi2 ] = acc1;

               zero  = ( acc1 == 0 );

               break;
            }
            case 'C': /* SUBC */
            {
               acc1 = reg[ opi2 ];
               if ( carry ) acc1--;

               carry = ( acc1 > 15 );
               acc1 = 0xf & acc1;
               reg[ opi2 ] = acc1;

               zero  = ( acc1 == 0 );

               break;
            }
            case 'D': /* DIN */
            {
               acc1 = 0x0;
               if ( in[0] ) acc1 |= 0x1;
               if ( in[1] ) acc1 |= 0x2;
               if ( in[2] ) acc1 |= 0x4;
               if ( in[3] ) acc1 |= 0x8;

               reg[ opi2 ] = acc1;

               carry = false;
               zero  = ( acc1 == 0 );

               break;
            }
            case 'E': /* DOT */
            {
               acc1 = reg[ opi2 ];
               out[0] = acc1 & 0x1;
               out[1] = acc1 & 0x2;
               out[2] = acc1 & 0x4;
               out[3] = acc1 & 0x8;

               carry = false;
               zero  = ( acc1 == 0 );

               break;
            }
            case 'F': /* KIN */
            {
               if ( stepmode ) break;

               keystroke = NONE;
               while ( keystroke == NONE || keystroke == CCE )
               {
                  UpdateW( false );
                  usleep(SLEEPTIME);
               }

               if ( keystroke == HALT )
               {
                  brk = true;
                  return;
               }

               if ( ! ( keystroke & 0xf0 ) )
               {
                  reg[ opi2 ] = keystroke;
                  zero  = ( reg[ opi2 ] == 0 );

                  keystroke = NONE;
               }

               carry = false;

               break;
            }

            default:
            {
               cerr << "Internal error: core2090 line "
                    << __LINE__ << "!" << endl;
            }
         }

         break;
      }

      default:
      {
         cerr << "Internal error: core2090 line "
              << __LINE__ << "!" << endl;
      }
   }

   addr[0] = a % 16;
   addr[1] = a >> 4;
}


void core2090::f_step()
{
   disp_no = 0;
   brk = false;
   keystroke = NONE;

   alu( true );

   if ( ! disout )
   {
      if ( disp_no == 0 ) get_line();
      else                get_regs();
   }

   disout = false;

   if ( register_dump ) reg_dump();

   return;
}


void core2090::f_run()
{
   struct timeval tv1, tv2;
   long udelay, udelta;

   disp_no = 0;
   disout = false;
   brk = false;
   keystroke = NONE;

   gettimeofday(&tv1, NULL);
   udelay = 0;

   while ( ( ! brk ) && ( keystroke != HALT ) )
   {
      if ( ! disout )
      {
         if ( disp_no == 0 ) get_line();
         else                get_regs();
      }
      UpdateW( false );

      if ( ! max_speed )
      {
         gettimeofday(&tv2, NULL);
         udelta = (tv2.tv_sec-tv1.tv_sec)*(long)1000000 + (tv2.tv_usec-tv1.tv_usec);
         tv1 = tv2;

         if ( disout )
            udelay += (long)10000 - udelta;
         else
            udelay += (long)21000 - udelta;

         if ( labs(udelay) > 200000 ) udelay = 0;

         if ( udelay > 0 ) usleep( (unsigned long)udelay );
      }

      alu( false );

      if ( register_dump ) reg_dump();

      if ( bkp[0] != 0 || bkp[1] != 0 )
         if ( addr[0] == bkp[0] && addr[1] == bkp[1] ) brk = true;
   }

   disout = false;

   return;
}


void core2090::f_bkp()
{
   unsigned int dig;
   Keys2090 last_key;

   dig = 0;
   last_key = NONE;

// set break point
   while ( dig != 2 )
   {
      usleep(SLEEPTIME);

      switch (keystroke)
      {
         case CCE:
         {
//            bkp[0] = bkp[1];
//            bkp[1] = 0x0;
            dig = 0;
            break;
         }

         case KEY_0:
         case KEY_1:
         case KEY_2:
         case KEY_3:
         case KEY_4:
         case KEY_5:
         case KEY_6:
         case KEY_7:
         case KEY_8:
         case KEY_9:
         case KEY_A:
         case KEY_B:
         case KEY_C:
         case KEY_D:
         case KEY_E:
         case KEY_F:
         {
            if ( dig > 0 ) bkp[1] = bkp[0];
            bkp[0] = keystroke;
            dig++;
            break;
         }

         case HALT: { return; }

         default: {}
      }
      if ( keystroke != NONE )
      {
         display[0] = bkp[1];
         display[1] = bkp[0];
         display[2] = 0xff;
         display[3] = 0x0;
         display[4] = 0x0;
         display[5] = 0x0;
         UpdateW();
         keystroke = NONE;
      }
   }

// change display
   addr[0] = bkp[0];
   addr[1] = bkp[1];

   UpdateW();
   display[4] = display[0];
   display[5] = display[1];

   while ( keystroke != HALT )
   {
      usleep(SLEEPTIME);

      switch (keystroke)
      {
         case CCE:
         {
            if ( last_key == CCE )
            {
               display[3] = 0x0;
               display[4] = 0x0;
               display[5] = 0x0;
            }
            else
            {
               display[5] = display[4];
               display[4] = display[3];
               display[3] = 0x0;
            }

            last_key = CCE;
            UpdateW();

            break;
         }

         case KEY_0:
         case KEY_1:
         case KEY_2:
         case KEY_3:
         case KEY_4:
         case KEY_5:
         case KEY_6:
         case KEY_7:
         case KEY_8:
         case KEY_9:
         case KEY_A:
         case KEY_B:
         case KEY_C:
         case KEY_D:
         case KEY_E:
         case KEY_F:
         {
            last_key = keystroke;

            display[3] = display[4];
            display[4] = display[5];
            display[5] = keystroke;

            UpdateW();

            break;
         }

         case RUN:
         case HALT: { return; }

         default: {}
      }

      keystroke = NONE;

   }

}


void core2090::get_line()
{
   int a;

   a = 16*addr[1] + addr[0];

   if ( a > 255 || a < 0 )
   {
      cerr << "Internal error: core2090 line "
           << __LINE__ << "!" << endl;
      reset();
   }

   display[0] = addr[1];
   display[1] = addr[0];
   display[2] = 0xff;
   display[3] = memory[a][0];
   display[4] = memory[a][1];
   display[5] = memory[a][2];
}

void core2090::get_dummy()
{
   display[0] = 0x0;
   display[1] = 0x0;
   display[2] = 0xff;
   display[3] = 0x0;
   display[4] = 0x0;
   display[5] = 0x0;
}

void core2090::get_regs()
{
   display[0] = display[1] = display[2]
              = display[3] = display[4]
              = display[5] = 0xff;

   if ( disp_no > 6 || disp_no < 1 )
   {
      cerr << "Internal error: core2090 line "
           << __LINE__ << "!" << endl;
      reset();
   }

   for ( int i=0; i<disp_no; i++ )
   {
      display[ 5-i ] = reg[ ( disp_reg + i ) % 16 ];
   }
}


void core2090::run()
{
   Keys2090 last_key;

   reset();

   last_key = NONE;

   go_on = true;

   while( go_on )
   {

      switch (keystroke)
      {
         case NEXT:
         {
            keystroke = NONE;
            f_next();
            break;
         }

         case REG:
         {
            keystroke = NONE;
            f_reg();
            get_line();
            keystroke = NONE;
            break;
         }

         case PGM:
         {
            keystroke = NONE;
            f_pgm();
            get_line();
            keystroke = NONE;
            break;
         }

         case STEP:
         {
            keystroke = NONE;
            f_step();
            keystroke = NONE;
            break;
         }

         case RUN:
         {
            keystroke = NONE;
            f_run();
            get_line();
            keystroke = NONE;
            break;
         }

         case BKP:
         {
            keystroke = NONE;
            f_bkp();
            keystroke = NONE;
            break;
         }

         case CCE:
         {
            if ( last_key == CCE )
            {
               display[3] = 0x0;
               display[4] = 0x0;
               display[5] = 0x0;
            }
            else
            {
               display[5] = display[4];
               display[4] = display[3];
               display[3] = 0x0;
            }

            last_key = CCE;

            keystroke = NONE;

            break;
         }

         case KEY_0:
         case KEY_1:
         case KEY_2:
         case KEY_3:
         case KEY_4:
         case KEY_5:
         case KEY_6:
         case KEY_7:
         case KEY_8:
         case KEY_9:
         case KEY_A:
         case KEY_B:
         case KEY_C:
         case KEY_D:
         case KEY_E:
         case KEY_F:
         {
            display[3] = display[4];
            display[4] = display[5];
            display[5] = keystroke;

            last_key = keystroke;

            keystroke = NONE;

            break;
         }

         case HALT: { get_line(); keystroke = NONE; }

         default: {}
      }

      if ( keystroke == NONE )
      {
         UpdateW();
         usleep(SLEEPTIME);
      }
   }

   exit();
}
