//
// convert.cpp
// Copyright (C) 7.8.2005 Ingo D. Rullhusen <d01c@uni-bremen.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//

#include <iostream>

using namespace std;


char ui2hex( unsigned char ui )
{
   switch( ui )
   {
      case 0x0: { return('0'); }
      case 0x1: { return('1'); }
      case 0x2: { return('2'); }
      case 0x3: { return('3'); }
      case 0x4: { return('4'); }
      case 0x5: { return('5'); }
      case 0x6: { return('6'); }
      case 0x7: { return('7'); }
      case 0x8: { return('8'); }
      case 0x9: { return('9'); }
      case 0xa: { return('A'); }
      case 0xb: { return('B'); }
      case 0xc: { return('C'); }
      case 0xd: { return('D'); }
      case 0xe: { return('E'); }
      case 0xf: { return('F'); }
   }
   return(' ');
}


unsigned char hex2ui( char hex )
{
   switch( hex )
   {
      case '0': { return(0x0); }
      case '1': { return(0x1); }
      case '2': { return(0x2); }
      case '3': { return(0x3); }
      case '4': { return(0x4); }
      case '5': { return(0x5); }
      case '6': { return(0x6); }
      case '7': { return(0x7); }
      case '8': { return(0x8); }
      case '9': { return(0x9); }
      case 'A':
      case 'a': { return(0xa); }
      case 'B':
      case 'b': { return(0xb); }
      case 'C':
      case 'c': { return(0xc); }
      case 'D':
      case 'd': { return(0xd); }
      case 'E':
      case 'e': { return(0xe); }
      case 'F':
      case 'f': { return(0xf); }
   }
   cerr << "Hex conversion error!" << endl;
   return(0xff);
}
