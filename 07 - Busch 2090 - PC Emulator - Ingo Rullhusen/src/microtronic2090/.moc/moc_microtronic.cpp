/****************************************************************************
** mainform meta object code from reading C++ file 'microtronic.h'
**
** Created: Sat Jul 8 07:46:53 2017
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.8   edited Feb 2 14:59 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "../.ui/microtronic.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.8b. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *mainform::className() const
{
    return "mainform";
}

QMetaObject *mainform::metaObj = 0;
static QMetaObjectCleanUp cleanUp_mainform( "mainform", &mainform::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString mainform::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "mainform", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString mainform::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "mainform", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* mainform::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QMainWindow::staticMetaObject();
    static const QUParameter param_slot_0[] = {
	{ "e", &static_QUType_ptr, "QKeyEvent", QUParameter::In }
    };
    static const QUMethod slot_0 = {"keyPressEvent", 1, param_slot_0 };
    static const QUParameter param_slot_1[] = {
	{ "e", &static_QUType_ptr, "QKeyEvent", QUParameter::In }
    };
    static const QUMethod slot_1 = {"keyReleaseEvent", 1, param_slot_1 };
    static const QUMethod slot_2 = {"reset_key_pressed", 0, 0 };
    static const QUMethod slot_3 = {"reset_key_released", 0, 0 };
    static const QUParameter param_slot_4[] = {
	{ "k", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_4 = {"HexKey_released", 1, param_slot_4 };
    static const QUParameter param_slot_5[] = {
	{ "k", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_5 = {"CKey_released", 1, param_slot_5 };
    static const QUMethod slot_6 = {"GetLoadProgramName", 0, 0 };
    static const QUMethod slot_7 = {"GetSaveProgramName", 0, 0 };
    static const QUParameter param_slot_8[] = {
	{ "e", &static_QUType_ptr, "QCustomEvent", QUParameter::In }
    };
    static const QUMethod slot_8 = {"customEvent", 1, param_slot_8 };
    static const QUParameter param_slot_9[] = {
	{ "state", &static_QUType_bool, 0, QUParameter::In }
    };
    static const QUMethod slot_9 = {"maxspeed_toggled", 1, param_slot_9 };
    static const QUMethod slot_10 = {"InConChanged", 0, 0 };
    static const QUParameter param_slot_11[] = {
	{ "state", &static_QUType_bool, 0, QUParameter::In }
    };
    static const QUMethod slot_11 = {"regdump_toggled", 1, param_slot_11 };
    static const QUParameter param_slot_12[] = {
	{ "state", &static_QUType_bool, 0, QUParameter::In }
    };
    static const QUMethod slot_12 = {"in1_toggled", 1, param_slot_12 };
    static const QUParameter param_slot_13[] = {
	{ "state", &static_QUType_bool, 0, QUParameter::In }
    };
    static const QUMethod slot_13 = {"in2_toggled", 1, param_slot_13 };
    static const QUParameter param_slot_14[] = {
	{ "state", &static_QUType_bool, 0, QUParameter::In }
    };
    static const QUMethod slot_14 = {"in3_toggled", 1, param_slot_14 };
    static const QUParameter param_slot_15[] = {
	{ "state", &static_QUType_bool, 0, QUParameter::In }
    };
    static const QUMethod slot_15 = {"in4_toggled", 1, param_slot_15 };
    static const QUParameter param_slot_16[] = {
	{ "state", &static_QUType_bool, 0, QUParameter::In }
    };
    static const QUMethod slot_16 = {"out1_toggled", 1, param_slot_16 };
    static const QUParameter param_slot_17[] = {
	{ "state", &static_QUType_bool, 0, QUParameter::In }
    };
    static const QUMethod slot_17 = {"out2_toggled", 1, param_slot_17 };
    static const QUParameter param_slot_18[] = {
	{ "state", &static_QUType_bool, 0, QUParameter::In }
    };
    static const QUMethod slot_18 = {"out3_toggled", 1, param_slot_18 };
    static const QUParameter param_slot_19[] = {
	{ "state", &static_QUType_bool, 0, QUParameter::In }
    };
    static const QUMethod slot_19 = {"out4_toggled", 1, param_slot_19 };
    static const QUMethod slot_20 = {"languageChange", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "keyPressEvent(QKeyEvent*)", &slot_0, QMetaData::Public },
	{ "keyReleaseEvent(QKeyEvent*)", &slot_1, QMetaData::Public },
	{ "reset_key_pressed()", &slot_2, QMetaData::Public },
	{ "reset_key_released()", &slot_3, QMetaData::Public },
	{ "HexKey_released(int)", &slot_4, QMetaData::Public },
	{ "CKey_released(int)", &slot_5, QMetaData::Public },
	{ "GetLoadProgramName()", &slot_6, QMetaData::Public },
	{ "GetSaveProgramName()", &slot_7, QMetaData::Public },
	{ "customEvent(QCustomEvent*)", &slot_8, QMetaData::Public },
	{ "maxspeed_toggled(bool)", &slot_9, QMetaData::Public },
	{ "InConChanged()", &slot_10, QMetaData::Public },
	{ "regdump_toggled(bool)", &slot_11, QMetaData::Public },
	{ "in1_toggled(bool)", &slot_12, QMetaData::Public },
	{ "in2_toggled(bool)", &slot_13, QMetaData::Public },
	{ "in3_toggled(bool)", &slot_14, QMetaData::Public },
	{ "in4_toggled(bool)", &slot_15, QMetaData::Public },
	{ "out1_toggled(bool)", &slot_16, QMetaData::Public },
	{ "out2_toggled(bool)", &slot_17, QMetaData::Public },
	{ "out3_toggled(bool)", &slot_18, QMetaData::Public },
	{ "out4_toggled(bool)", &slot_19, QMetaData::Public },
	{ "languageChange()", &slot_20, QMetaData::Protected }
    };
    metaObj = QMetaObject::new_metaobject(
	"mainform", parentObject,
	slot_tbl, 21,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_mainform.setMetaObject( metaObj );
    return metaObj;
}

void* mainform::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "mainform" ) )
	return this;
    return QMainWindow::qt_cast( clname );
}

bool mainform::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: keyPressEvent((QKeyEvent*)static_QUType_ptr.get(_o+1)); break;
    case 1: keyReleaseEvent((QKeyEvent*)static_QUType_ptr.get(_o+1)); break;
    case 2: reset_key_pressed(); break;
    case 3: reset_key_released(); break;
    case 4: HexKey_released((int)static_QUType_int.get(_o+1)); break;
    case 5: CKey_released((int)static_QUType_int.get(_o+1)); break;
    case 6: GetLoadProgramName(); break;
    case 7: GetSaveProgramName(); break;
    case 8: customEvent((QCustomEvent*)static_QUType_ptr.get(_o+1)); break;
    case 9: maxspeed_toggled((bool)static_QUType_bool.get(_o+1)); break;
    case 10: InConChanged(); break;
    case 11: regdump_toggled((bool)static_QUType_bool.get(_o+1)); break;
    case 12: in1_toggled((bool)static_QUType_bool.get(_o+1)); break;
    case 13: in2_toggled((bool)static_QUType_bool.get(_o+1)); break;
    case 14: in3_toggled((bool)static_QUType_bool.get(_o+1)); break;
    case 15: in4_toggled((bool)static_QUType_bool.get(_o+1)); break;
    case 16: out1_toggled((bool)static_QUType_bool.get(_o+1)); break;
    case 17: out2_toggled((bool)static_QUType_bool.get(_o+1)); break;
    case 18: out3_toggled((bool)static_QUType_bool.get(_o+1)); break;
    case 19: out4_toggled((bool)static_QUType_bool.get(_o+1)); break;
    case 20: languageChange(); break;
    default:
	return QMainWindow::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool mainform::qt_emit( int _id, QUObject* _o )
{
    return QMainWindow::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool mainform::qt_property( int id, int f, QVariant* v)
{
    return QMainWindow::qt_property( id, f, v);
}

bool mainform::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
