//
// stdin_thread.cpp
// Copyright (C) 8.10.2005 Ingo D. Rullhusen <d01c@uni-bremen.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//

#include <unistd.h>
#include <fcntl.h>
#include <sys/select.h>

#include <qapplication.h>
#include <qcheckbox.h>

#include "stdin_thread.h"
#include "MyEvents.h"
#include "convert.h"
#include "Combos.h"

#include "microtronic.h"
extern QApplication *pA;
extern mainform *pW;


void STDIN_thread::run()
{
   unsigned char uc;
   int in_char;
   QCustomEvent *e;
   fd_set rfds;
   struct timeval tv;

   go_on = true;

   while ( getchar() > -1 && go_on ){};

   while( go_on )
   {
      FD_ZERO(&rfds);
      FD_SET(fd, &rfds);
      tv.tv_sec = 0;
      tv.tv_usec = 10000;

      if ( select(1, &rfds, NULL, NULL, &tv) )
         in_char = getchar();
      else
         continue;

      uc = ( in_char > 31 && in_char < 128 ? hex2ui( in_char ) : 16 );
      if ( uc < 16 )
      {
         pA->lock();

         switch ( pW->InCon->currentItem() )
         {
            case STDIN:
               pW->in1->setChecked( uc & 0x1 );
               pW->in2->setChecked( uc & 0x2 );
               pW->in3->setChecked( uc & 0x4 );
            case CLKF12STDIN_ON_14:
               pW->in4->setChecked( uc & 0x8 );
         }

         pA->unlock();

         e = new QCustomEvent(UPDATE_EVENT);
         QApplication::postEvent( pW, e );   // *e deleted by postEvent
      }
   }

   exit();
}


STDIN_thread::STDIN_thread()
{
   tcgetattr(fd, &oterm);
   memcpy(&term, &oterm, sizeof(term));
   term.c_lflag = term.c_lflag & (~ICANON) & (~ECHO); /* disable sp. chars, buffer, echo */
   term.c_cc[VMIN]  = 0; /* min. number of characters for ~ICANON */
   term.c_cc[VTIME] = 0; /* timeout in deciseconds for ~ICANON */
   tcsetattr(fd, TCSANOW, &term);

   ofstatus = fcntl(fd, F_GETFL, 0);
   fcntl( fd, F_SETFL, ofstatus | O_NONBLOCK );
}

STDIN_thread::~STDIN_thread()
{
   while ( getchar() > -1 ){};
   tcsetattr(fd, TCSANOW, &oterm);

   fcntl(fd, F_SETFL, ofstatus);
}
