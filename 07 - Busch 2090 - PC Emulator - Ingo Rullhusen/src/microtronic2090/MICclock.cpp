//
// MICclock.cpp
// Copyright (C) 22.10.2005 Ingo D. Rullhusen <d01c@uni-bremen.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//

#include <qdatetime.h>
#include <qcheckbox.h>
#include <qapplication.h>

#include "microtronic.h"
#include "MICclock.h"
#include "Combos.h"
#include "MyEvents.h"
#include "convert.h"


using namespace std;

extern QApplication *pA;
extern mainform *pW;


#define SLEEPTIME 10


clock2090::clock2090()
{
   time_val.lock();
   h2 = h1 = m2 = m1 = s2 = s1 = 0;
   time_val.unlock();
}


void clock2090::run()
{
   QTime last_time;
   QCustomEvent *e;

   time_val.lock();
   h2 = h1 = m2 = m1 = s2 = s1 = 0;
   time_val.unlock();

   go_on = true;

   last_time.start();
   while( go_on )
   {

// clock
      if ( last_time.elapsed() > 500 )
      {
         pA->lock();

         pW->clock->toggle();
         last_time = last_time.addMSecs( 500 );

         switch ( pW->InCon->currentItem() )
         {
            case CLOCK_ON_1:
            case CLKF12STDIN_ON_14:
            {
               pW->in1->setChecked( ! pW->clock->isChecked() );
               break;
            }
            case CLOCK_ON_2:
            {
               pW->in2->setChecked( ! pW->clock->isChecked() );
               break;
            }
            case CLOCK_ON_3:
            {
               pW->in3->setChecked( ! pW->clock->isChecked() );
               break;
            }
            case CLOCK_ON_4:
            case F13CLOCK_ON_14:
            {
               pW->in4->setChecked( ! pW->clock->isChecked() );
               break;
            }
            default: {}
         }

         pA->unlock();
      }
      else
      {
         msleep(SLEEPTIME);
      }

      e = new QCustomEvent(UPDATE_EVENT);
      QApplication::postEvent( pW, e );   // *e deleted by postEvent

// output
      switch ( pW->OutCon->currentItem() )
      {
         case BUZZER_ON_1:
         {
            if ( pW->out1->isChecked() && go_on )
            {
               pA->lock();
               QApplication::beep();
               pA->unlock();
            }
            break;
         }
         case BUZZER_ON_2:
         {
            if ( pW->out2->isChecked() && go_on )
            {
               pA->lock();
               QApplication::beep();
               pA->unlock();
            }
            break;
         }
         case BUZZER_ON_3:
         {
            if ( pW->out3->isChecked() && go_on )
            {
               pA->lock();
               QApplication::beep();
               pA->unlock();
            }
            break;
         }
         case BUZZER_ON_4:
         {
            if ( pW->out4->isChecked() && go_on )
            {
               pA->lock();
               QApplication::beep();
               pA->unlock();
            }
            break;
         }

         default: {}
      }

   }

   exit();
}


void clock2090::increment()
{
   time_val.lock();

   s1++;
   if ( s1 > 9 )
   {
      s1 = 0;
      s2++;
      if ( s2 > 5 )
      {
         s2 = 0;
         m1++;
         if ( m1 > 9 )
         {
            m1 = 0;
            m2++;
            if ( m2 > 5 )
            {
               m2 = 0;
               h1++;
               if ( h2 > 1 && h1 > 3 )
               {
                  h2 = h1 = 0; 
               }
               else if ( h1 > 9 )
               {
                  h1 = 0;
                  h2++;
                  if ( h2 > 2 )
                  {
                     h2 = 0;
                  }
               }
            }
         }
      }
   }

   time_val.unlock();
}
