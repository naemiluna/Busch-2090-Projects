# Micsim - A Busch 2090 PC Emulator

written by Ingo Rullhusen

Notes written 8. July 2017, Martin Sauter

![Project Image](https://github.com/martinsauter/Busch-2090-Projects/blob/master/07%20-%20Busch%202090%20-%20PC%20Emulator%20-%20Ingo%20Rullhusen/Ingo%20Rullhusen%20-%20Busch%202090%20Simulator.png)

Image: Ingo Rullhusen's Busch 2090 simulator next to Martin Sauter's Busch 2090 assembler

### Overview

* The source and executable (from 2005) require qt3 support which was last included in Ubuntu 12.04. In newer versions, qt3 support to compile and execute is no longer included. 

* To get the emulator running, set up an Ubuntu 12.04 virtual machine and install the necessary qt3 development libraries.
 
* Due to file size limitations a VM image with a ready to run installation can't be provided on Github. However, I'm happy to make the Virtual Machine Container (1.9 GB) in OVA1 Format available on request if creating a VM client with an Ubuntu 12.04 image is not possible anymore at some point in time.

### Installation from scratch (alrady done in VM image)

* Install required libraries: 

sudo apt-get install qt3-default
sudo apt-get install qt3-dev-tools
sudo apt-get install build-essential

* Untar micsim-0.0.5-compiled-ubuntu1204-qt3.8.5.tar in Ubuntu 12.04 in a directory. In the resulting directory, go to the 'microtronic2090' folder and execute './microtronic', Note: The executable provided in this tar file is for a 64-bit Ubuntu 12.04!
 
* The "Nimm-game" can be loaded as on the original with "HALT-PGM-7-HALT-NEXT-00-RUN". Then type in eg. 17, 3, and then 1 to take one stick.
 
* Load a program from a .mic file: Press "HALT-PGM1" to open a file load dialog box. In the original version there must be NO empty lines. Each line has to contain three hex digit and a \r at the end! It's o.k. not to have 0xFF lines.
 

### Compile from scratch (alrady done in VM image)

* If not already done install the libraries mentioned above.

* Modify 'Makefile' in the root directory: Path to qmake: QMAKE := /usr/bin/qmake (instead of environment variable
 
* Delete 'Makefile" in the microtronic2090 folder!
 
Now compile as follows:

* Go to root directory of the project

* Run 'make clean' (delete .o files of previous runs, required when compiled first time in new target architecture)

* Run 'make'
